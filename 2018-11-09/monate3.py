x = input("Bitte geben Sie ein Zahl ein")
x = int(x)

def monat(y):
    if y==1:
        monat ="Januar"
    elif y == 2:
        monat = "Februar"
    elif y == 3:
        monat = "März"
    else:
        monat = "Falsche Eingabe"
    
    return monat

# info: eine Funktion muss immer mittels f() aufgerufen werden
# sonst: ausgabe von "<function ....>"
print(monat(x))
