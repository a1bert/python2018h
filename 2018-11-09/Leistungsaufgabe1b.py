from turtle import *

##Schweizerkreuz b)
hideturtle()
speed(10)

sl_float = numinput("Eingabe", "Bitte geben Sie die Seitenlänge ein")
sl = int(sl_float)

penup()
rt(90)
fd(200)
rt(90)
fd(200)
pendown()

##Roter Rahmen
fillcolor("red")
pencolor('white')
begin_fill()
rt(90);fd(400)
rt(90);fd(400)
rt(90);fd(400)
rt(90);fd(400)
end_fill()

#Weisses Kreuz
penup()
rt(90);fd((400-sl)/2);rt(90);fd((400-(3*sl))/2)
pendown()
fillcolor('white')
begin_fill()
fd(sl);rt(90);fd(sl);lt(90);fd(sl);lt(90)
fd(sl);rt(90);fd(sl);lt(90);fd(sl);lt(90)
fd(sl);rt(90);fd(sl);lt(90);fd(sl);lt(90)
fd(sl);rt(90);fd(sl);lt(90);fd(sl);lt(90)
end_fill()

exitonclick()