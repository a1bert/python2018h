def notendurchschnitt(name, n1, n2, n3, n4, n5, n6):
    print(name, "Notendurchschnitt")
    durchschnitt =  (n1 + n2 + n3 + n4 + n5 + n6) / 6
    return round(durchschnitt, 2)

def notendurchschnitt2(name, n1, n2, n3, n4, n5, n6):
    print(name, "Notendurchschnitt")
    return round((n1 + n2 + n3 + n4 + n5 + n6) / 6, 2)

def notendurchschnitt3(name, n1, n2, n3, n4, n5, n6):
    durchschnitt =  (n1 + n2 + n3 + n4 + n5 + n6) / 6
    name = name + ", Notendurchschnitt: "
    return name + str(round(durchschnitt, 2))

def notendurchschnitt4(name, n1, n2, n3, n4, n5, n6):
    durchschnitt =  (n1 + n2 + n3 + n4 + n5 + n6) / 6
    ausgabe = name + ", Notendurchschnitt: " + str(round(durchschnitt, 2))
    return ausgabe

durchschnitt = notendurchschnitt("Max Mustermann", 4, 4.5, 5.5, 6, 6, 6)
print(durchschnitt)
