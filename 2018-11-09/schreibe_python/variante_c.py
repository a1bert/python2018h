from buchstaben import *

start_position()

stift_einstellungen = pen()

schreibe_p1_fixed()
pen(stift_einstellungen)

schreibe_y1()
pen(stift_einstellungen)

schreibe_t1_fixed()
pen(stift_einstellungen)

schreibe_h2()
pen(stift_einstellungen)

schreibe_o2()
pen(stift_einstellungen)

schreibe_n3()
pen(stift_einstellungen)

exitonclick()
