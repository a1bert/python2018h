#
# Turtle Befehle zum Zeichnen der Buchstaben für das Wort
# Python.
#

from turtle import *

def start_position():
    up()
    goto(-900,0)
    down()

def schreibe_o1():
    goto(0, -200)
    fillcolor("grey")
    begin_fill()
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    end_fill()

    penup()
    bk(80)
    lt(90)
    fd(220)
    pendown()
    pencolor("pink")
    pensize(40)
    circle(75, 180)
    fd(40)
    circle(75, 180)
    fd(40)
    penup()
    bk(220)
    rt(90)
    fd(80)


# Buchstabe H, Variante Kleinbuchstabe
# von Desiree Rust, Regina Eicher und Jonathan Hoitink

# schreiben des Buchstaben h mittels Funktion
def schreibe_h1():
    fillcolor("pink")
    begin_fill()
    fd(300); lt(90); fd(400); lt(90); fd(300); lt(90); fd(400); lt(90)
    end_fill()

    penup()
    fd(60); lt(90); fd(60)
    pendown()

    fillcolor("black")
    begin_fill()
    fd(240); rt(40); fd(60); rt(140); fd(285); rt(90); fd(40)
    end_fill()

    penup()
    back(40); rt(90); fd(90)
    pendown()

    pensize(2)
    fillcolor("orange")
    begin_fill()
    circle(-60, 180); fd(92); rt(90); fd (25); rt(90); fd(90); circle (47, 180);
    end_fill()

    penup()
    forward(150)
    lt(90)
    forward(200)
    pendown()


def schreibe_h2():

    fd(300); lt(90); fd(400); lt(90); fd(300); lt(90); fd(400); lt(90);
    penup()
    fd(60); lt(90); fd(60)
    pendown()
    pensize(4)
    pencolor("lime")
    fillcolor("cyan")
    begin_fill()
    fd(300); rt(90); fd(60); rt(90); fd(100); lt(90); fd(60); lt(90); fd(100); rt(90); fd(60); rt(90);
    fd(300); rt(90); fd(60); rt(90); fd(100); lt(90); fd(60); lt(90); fd(100); rt(90); fd(60);
    end_fill()
    penup()
    lt(90); fd(60); lt(90); fd(240)
    pendown()
    penup()

# Autor: Almira Medaric
# Datum: 05.11.2018

#funktion
def schreibe_n(vdia, vhoehe, dlaenge, color1, color2, color3):
    #rahmen
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    #im papier
    penup()
    fd(25)
    lt(90)
    fd(70)
    pendown()
    #erste vertikale
    pencolor(color2)
    fillcolor(color1)
    begin_fill()
    rt(45)
    fd(vdia)
    lt(45)
    fd(vhoehe)
    lt(135)
    fd(vdia)
    lt(45)
    fd(vhoehe)
    end_fill()
    penup()
    bk(200)
    lt(45)
    pendown()
    #diagonale
    pencolor(color3)
    fillcolor(color2)
    begin_fill()
    fd(80000**(1/2))
    lt(90)
    fd(vdia)
    lt(90)
    fd(80000**(1/2))
    lt(90)
    fd(vdia)
    lt(90)
    penup()
    fd(80000**(1/2))
    pendown()
    end_fill()
    #zweite vertikale
    pencolor(color1)
    fillcolor(color3)
    begin_fill()
    lt(90)
    fd(vdia)
    lt(45)
    fd(vhoehe)
    lt(135)
    fd(vdia)
    lt(45)
    fd(vhoehe)
    end_fill()
    # plazierung für neuen Blatt
    penup()
    fd(70)
    left(90)
    fd(75)
    pendown()

def schreibe_n1():
    schreibe_n(5000**(1/2), 200, 80000**(1/2), "light blue", "pink", "yellow")

def schreibe_n2():
    for i in range (2):
        forward(300)
        left(90)
        forward(400)
        left(90)
    penup()
    left(90)
    forward (50)
    right(90)
    forward (50)
    pendown()
    fillcolor("black")
    begin_fill()
    for i in range (2):
        forward (200)
        left(90)
        forward(300)
        left(90)
    end_fill()
    penup()
    left(90)
    forward (50)
    right(90)
    forward (50)
    pendown()
    pensize (20)
    pencolor("white")
    left(90)
    forward(200)
    right(153.43494882)
    forward(223.60679775)
    left(153.43494882)
    forward(200)
    pencolor("black")
    right(90)
    penup()
    forward(100)
    right(90)
    forward(300)
    left(90)
    pendown()
    pensize(1)


def schreibe_o2():
    pensize(5)
    pencolor("black")
    fillcolor("cyan")
    begin_fill()
    fd(300); lt(90); fd(400); lt(90); fd(300); lt(90); fd(400); lt(90)
    end_fill()
    pencolor("red")
    pensize(30)
    pu(); fd(150); lt(90); fd(180); lt(90); fd(100); lt(90); pd()
    circle(100, 180); fd(40); circle(100,180); fd(40);
    lt(90); pu(); fd(100); rt(90); fd(180); lt(90); fd(150); pd()

def schreibe_p1():
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup() #Ausgansposition
    fd(120)
    left(90)
    forward(40)
    pendown()

    pensize(40) #Buchstabe Strich
    pencolor("red")
    forward(300)

    right(90)
    circle(-90, 180) #Buchstabe Bauch

    penup() #Endposition
    pendown()

def schreibe_p1_fixed():
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup() #Ausgansposition
    fd(120)
    left(90)
    forward(40)
    pendown()

    pensize(40) #Buchstabe Strich
    pencolor("red")
    forward(300)

    right(90)
    circle(-90, 180) #Buchstabe Bauch

    penup() #Endposition
    left(90)
    forward(340-180)
    left(90)
    forward(300-120)
    pendown()



def schreibe_p2():
    #goto(0,-200)
    #Rahmen
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    #P in einem Strich
    fillcolor("lawn green"); begin_fill(); pencolor("green")
    penup();fd(80);pendown();lt(90);fd(370);rt(90);fd(30); circle(-100, 180)
    #inneres P
    lt(90);fd(30);lt(90);circle(70,195);lt(70);goto(130,0)

    end_fill()
    penup()
    #in ausgangsposition
    goto(300,0);lt(90)
    pendown()

def schreibe_t1():

    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup()
    forward(140)

    left(90)
    forward(20)
    pendown()

    fillcolor("cyan")
    begin_fill()

    forward(250)

    left(90)
    forward(100)

    right(90)
    forward(20)

    right(90)
    forward(220)

    right(90)
    forward(20)

    right(90)
    forward(100)

    left(90)
    forward(250)

    right(90)
    forward(20)

    end_fill()

    # ab hier Schatten

    fillcolor("blue")

    begin_fill()
    left(135)
    forward(10)
    left(45)
    forward(20)
    left(135)
    forward(10)
    end_fill()

    begin_fill()
    backward(10)
    right(45)
    forward(250)
    left(45)
    forward(10)
    end_fill()

    begin_fill()
    backward(10)
    right(135)
    forward(100)
    left(135)
    forward(10)
    end_fill()

    begin_fill()
    backward(10)
    right(45)
    forward(20)
    left(45)
    forward(10)
    end_fill()

    penup()
    right(135)
    forward(40)
    right(90)
    forward(290)

def schreibe_t1_fixed():
    schreibe_t1()
    # correct pen position
    left(90)

 # by Remo Bischoff and Florian Riedmann
def schreibe_t2():
    fd(30)
    lt(90); fd(250)
    rt(90); fd(100)
    lt(90); fd(30)
    lt(90); fd(230)
    lt(90); fd(30)
    lt(90); fd(100)
    rt(90); fd(250)

    lt(90)
     # Schatten
    up()
    goto(145, -160)
    down()

    fd(30);
    lt(90); fd(250)
    rt(90); fd(100)
    lt(90); fd(30)
    lt(45); fd(14)
    lt(45); fd(230)
    lt(90); fd(30)
    lt(45); fd(14)
    lt(45); fd(90)
    rt(90); fd(240)
    lt(45); fd(14)
    lt(45)

    up()
    goto(0,-200)
    down()

    # Rahmen
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    fd(300) # Turtle goes to start


def schreibe_y1():
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    fillcolor("lightgreen")

    penup()
    fd(125)
    left(90)
    fd(50)
    pendown()
    begin_fill()
    fd(150)
    left(45)
    fd(100)
    right(135)
    fd(30)
    right(45)
    fd(70)
    left(90)
    fd(70)
    left(315)
    fd(30)
    left(225)
    fd(100)
    right(315)
    fd(150)
    right(90)
    fd(17)
    end_fill()
    penup()
    left(90)
    fd(50)
    left(90)
    fd(175)
    pendown()


def schreibe_y2():

    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup() #Ausgansposition
    fd(150)
    left(90)
    forward(80)
    pendown()

    pensize(40) #Buchstabe Teil 1
    pencolor("pink")
    forward(150)
    left(45)
    forward(100) #Buchstabe Teil 2

    penup()
    back(100)
    pendown() # zurück zu Buchstabenmitte

    right(90)
    forward(100) #Buchstabe Teil 3

    penup() #Endposition
    left(180)
    forward(100)
    left(45)
    forward(230)
    left(90)
    forward(150)
    pendown()

def schreibe_n3():
    fillcolor("yellow")
    begin_fill()
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    end_fill()

    penup()
    fd(100)
    lt(90)
    fd(100)

    pendown()
    pensize(50)
    pencolor("blue")
    fd(200)
    rt(150)
    fd(235)
    lt(150)
    fd(210)

    penup()
    rt(180)
    fd(305)
    lt(90)
    fd(88)
    hideturtle()
