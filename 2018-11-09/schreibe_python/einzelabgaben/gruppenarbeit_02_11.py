print("Hello World!")
# author: desiree rust
# gruppenarbeit "H"

from turtle import *

reset()
goto(0,-200) # Rechteck im Sichtfeld positionieren


def schreibe_H():
    
    fd(300); lt(90); fd(400); lt(90); fd(300); lt(90); fd(400); lt(90);  
    penup()
    fd(60); lt(90); fd(60)
    pendown()
    pensize(4)
    pencolor("lime")
    fillcolor("cyan")
    begin_fill()
    fd(300); rt(90); fd(60); rt(90); fd(100); lt(90); fd(60); lt(90); fd(100); rt(90); fd(60); rt(90); 
    fd(300); rt(90); fd(60); rt(90); fd(100); lt(90); fd(60); lt(90); fd(100); rt(90); fd(60);  
    end_fill()
    penup()
    lt(90); fd(60); lt(90); fd(240)
    pendown()
    penup()
    
H = schreibe_H()

# turtle befindet sich nicht in "home"-position

exitonclick()