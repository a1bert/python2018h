from turtle import *
 # by Remo Bischoff and Florian Riedmann
reset()
shape("turtle")
#AUSKOMMENTIEREN
up()
goto(0,-200)
down()
#AUSKOMMENTIEREN
def turtles_rule():
 up()
 goto(135, -150)
 down()
 
 fd(30)
 lt(90); fd(250)
 rt(90); fd(100)
 lt(90); fd(30)
 lt(90); fd(230)
 lt(90); fd(30)
 lt(90); fd(100)
 rt(90); fd(250) 
 
 lt(90)
  # Schatten
 up()
 goto(145, -160)
 down()
 
 fd(30);
 lt(90); fd(250)
 rt(90); fd(100)
 lt(90); fd(30)
 lt(45); fd(14)
 lt(45); fd(230)
 lt(90); fd(30)
 lt(45); fd(14)
 lt(45); fd(90)
 rt(90); fd(240)
 lt(45); fd(14)
 lt(45)
 
 up()
 goto(0,-200)
 down()
 
# Rahmen
 fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
 fd(300) # Turtle goes to start
 

turtles_rule() 

exitonclick()