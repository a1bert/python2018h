from turtle import *

# Buchstabe N

reset()
##speed(10)
##goto(0, -250)

def schreibe_n():
    fillcolor("yellow")
    begin_fill()
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    end_fill()

    penup()
    fd(100)
    lt(90)
    fd(100)
    
    pendown()
    pensize(50)
    pencolor("blue")
    fd(200)
    rt(150)
    fd(235)
    lt(150)
    fd(210)
    
    penup()
    rt(180)
    fd(305)
    lt(90)
    fd(88)
    hideturtle()

print(schreibe_n())
    
     
     
     
