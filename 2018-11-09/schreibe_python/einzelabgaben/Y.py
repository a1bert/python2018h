from turtle import*
#goto(0, -200)

def schreibe_y():
    
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup() #Ausgansposition
    fd(150)
    left(90)
    forward(80)
    pendown()

    pensize(40) #Buchstabe Teil 1
    pencolor("pink")
    forward(150)
    left(45)
    forward(100) #Buchstabe Teil 2

    penup()
    back(100)
    pendown() # zurück zu Buchstabenmitte

    right(90)
    forward(100) #Buchstabe Teil 3

    penup() #Endposition
    left(180)
    forward(100)
    left(45)
    forward(230)
    left(90)
    forward(150)
    pendown()

schreibe_y()