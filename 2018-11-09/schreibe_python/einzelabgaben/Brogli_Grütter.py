
from turtle import *

def schreibe_o():
    goto(0, -200)
    fillcolor("grey")
    begin_fill()
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    end_fill()
    
    penup()
    bk(80)
    lt(90)
    fd(220)
    pendown()
    pencolor("pink")
    pensize(40)
    circle(75, 180)
    fd(40)
    circle(75, 180)
    fd(40)
    penup()
    bk(220)
    rt(90)
    fd(80)
    
 
schreibe_o()
exitonclick()

