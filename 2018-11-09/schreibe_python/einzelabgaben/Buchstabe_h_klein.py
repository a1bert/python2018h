# Buchstabe H, Variante Kleinbuchstabe
# von Desiree Rust, Regina Eicher und Jonathan Hoitink

from turtle import*
reset()


goto(0,-200) # Damit Rahmen (Rechteck) im Sichtfeld positioniert ist

# schreiben des Buchstaben h mittels Funktion
def buchstabe_h():
    fillcolor("pink")
    begin_fill()
    fd(300); lt(90); fd(400); lt(90); fd(300); lt(90); fd(400); lt(90)
    end_fill()
    
    penup()
    fd(60); lt(90); fd(60)
    pendown()
    
    fillcolor("black")
    begin_fill()
    fd(240); rt(40); fd(60); rt(140); fd(285); rt(90); fd(40)
    end_fill()
    
    penup()
    back(40); rt(90); fd(90)
    pendown()
    
    pensize(2)
    fillcolor("orange")
    begin_fill()
    circle(-60, 180); fd(92); rt(90); fd (25); rt(90); fd(90); circle (47, 180);
    end_fill()
    
    penup()
    forward(150)
    lt(90)
    forward(200)
    pendown()
    
buchstabe_h()
    
exitonclick()
