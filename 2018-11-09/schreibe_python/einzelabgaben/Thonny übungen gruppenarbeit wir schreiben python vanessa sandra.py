from turtle import *

reset()

penup()
goto (-100, -200)
pendown()

def dreieck():

    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup()
    forward(140)

    left(90)
    forward(20)
    pendown()

    fillcolor("cyan")
    begin_fill()

    forward(250)

    left(90)
    forward(100)

    right(90)
    forward(20)

    right(90)
    forward(220)

    right(90)
    forward(20)

    right(90)
    forward(100)

    left(90)
    forward(250)

    right(90)
    forward(20)

    end_fill()

    # ab hier Schatten

    fillcolor("blue")
    
    begin_fill()
    left(135)
    forward(10)
    left(45)
    forward(20)
    left(135)
    forward(10)
    end_fill()

    begin_fill()
    backward(10)
    right(45)
    forward(250)
    left(45)
    forward(10)
    end_fill()

    begin_fill()
    backward(10)
    right(135)
    forward(100)
    left(135)
    forward(10)
    end_fill()

    begin_fill()
    backward(10)
    right(45)
    forward(20)
    left(45)
    forward(10)
    end_fill()
    
    penup()
    right(135)
    forward(40)
    right(90)
    forward(290)
    
    
dreieck()

exitonclick()