#Wir zeichnen P-Y-T-H-O-N
#P

from turtle import *
from math import pi
def buchstabe_p():
    #goto(0,-200)
    #Rahmen
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    #P in einem Strich
    fillcolor("lawn green"); begin_fill(); pencolor("green")
    penup();fd(80);pendown();lt(90);fd(370);rt(90);fd(30); circle(-100, 180)
    #inneres P
    lt(90);fd(30);lt(90);circle(70,195);lt(70);goto(130,0)

    end_fill()
    penup()
    #in ausgangsposition
    goto(300,0);lt(90)
    pendown()

buchstabe_p()
exitonclick()
