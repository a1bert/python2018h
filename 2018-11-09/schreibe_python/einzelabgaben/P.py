from turtle import*
#goto(0, -200)

def schreibe_p():
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    penup() #Ausgansposition
    fd(120)
    left(90)
    forward(40)
    pendown()

    pensize(40) #Buchstabe Strich
    pencolor("red")
    forward(300)

    right(90)
    circle(-90, 180) #Buchstabe Bauch

    penup() #Endposition
    pendown()

schreibe_p()