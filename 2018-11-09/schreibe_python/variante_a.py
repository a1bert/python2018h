from buchstaben import *

start_position()

stift_einstellungen = pen()

schreibe_p1()
pen(stift_einstellungen)

# minimaler fix damit wir die weiteren buchstaben sehen
left(180)

schreibe_y1()
pen(stift_einstellungen)

schreibe_t1()
pen(stift_einstellungen)
# minimaler fix damit wir die weiteren buchstaben sehen
left(90)


schreibe_h1()
pen(stift_einstellungen)

schreibe_o1()
pen(stift_einstellungen)

schreibe_n1()
pen(stift_einstellungen)

exitonclick()
