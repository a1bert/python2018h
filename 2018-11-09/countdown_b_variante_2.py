# ein shortcut

i = 10

# zahlen von 10 - 6 ausgeben
while i > 5:
    print(i)
    i = i - 1
    
print("5 nur noch 5 Sekunden!")

# zahlen von 4 bis 2 ausgeben
i = i - 1
while i > 1:
    print(i)
    i = i - 1

print("1 gleich ist es so weit!")
print("0")
print("finito")
