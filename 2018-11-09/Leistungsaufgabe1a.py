from turtle import *

##Schweizerkreuz a)
hideturtle()
speed(10)

penup()
rt(90)
fd(200)
rt(90)
fd(200)
pendown()

##Roter Rahmen
fillcolor("red")
pencolor('white')
begin_fill()
rt(90);fd(400)
rt(90);fd(400)
rt(90);fd(400)
rt(90);fd(400)
end_fill()

#Weisses Kreuz
penup()
rt(90);fd(150);rt(90);fd(50)
pendown()
fillcolor('white')
begin_fill()
fd(100);rt(90);fd(100);lt(90);fd(100);lt(90)
fd(100);rt(90);fd(100);lt(90);fd(100);lt(90)
fd(100);rt(90);fd(100);lt(90);fd(100);lt(90)
fd(100);rt(90);fd(100);lt(90);fd(100);lt(90)
end_fill()

