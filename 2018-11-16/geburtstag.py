for name in "Ana", "Tom", "Tim", "Joe":
    # wird für jeden namen aufgerufen
    # gratuliert 3x
    for count in range(3):
        if count == 2:
            print("Und jetzt zum letzten Mal...")
        print("Und", count+1, "- Alles Gute zum Geburtstag", name+"!")
    
    print()


print("Alternative:")

def gratuliere(name):
    for count in range(3):
        if count == 2:
            print("Und jetzt zum letzten Mal...")
    print("Und", count+1, "- Alles Gute zum Geburtstag", name+"!")
    print()
    
for name in "Ana", "Tom", "Tim", "Joe":
    gratuliere(name)