# zeichen werden ersetzt durch *


input_orig = input("bitte einfach etwas eingeben: ")
zeichenkette = input_orig
i = len(zeichenkette)
zeichenkette = zeichenkette.replace(zeichenkette, i * "*")
print("Lösung 1:", zeichenkette)

# lösung 2
z = input_orig
print(z)
for no, bst in enumerate(z):
    z = z.replace(bst, '*')
print("Lösung 2:", z)

# lösung 3
zeichenkette = input_orig
anzahl = len(zeichenkette)
print("Lösung 3:", anzahl * "*")

# lösung 4
z = input_orig
lsg = ""
for no, bst in enumerate(z):
    if bst in ["a", "e", "i", "o", "u", "ä", "ö", "ü"]:
        lsg = lsg + "x"
    else:
        lsg = lsg + "*"
        
print("Lösung 4:", lsg)


# listenelemente werden ersetzt durch sterne
liste1 = ["heute", "ist", "ein", "guter", "tag"]
liste1[0:5] = "*" *5
print(liste1)

