liste = [ ["Stefanie", 123], ["Remo", 456], ["Tamara", 789]]
liste[1][1] = 345

# variante mit enumerate => wir erhalten nummer + tuple
for no, zeile in enumerate(liste):
    print(zeile)
    
# direkte variante - zwei werte pro zeile werden den entsprechenden
# variablen zugeordnet
for name, tel in liste:
    print(name, tel)

for zeile in liste:
    print(zeile)
    
    name = zeile[0]
    tel = zeile[1]
    print(name, tel)
    
for name, tel in liste:
    print(tel)