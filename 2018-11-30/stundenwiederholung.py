# Stundenwiederholung vom 30. November 2018

# liste erstellt
liste=["Stefanie", "Remo", "Tamara"]

# liste duplizieren
liste2=liste+liste
liste3=liste*2

# letzten beiden namen löschen
liste.remove("Tamara")
liste.remove("Remo")

# letzten beiden namen ohne diese zu kennen löschen
liste=["Stefanie", "Remo", "Tamara"]
del liste[1:3]

liste4=["Stefanie", "Remo", "Tamara"]
del liste4[-2:]




liste=["Stefanie", "Remo", "Tamara"]
liste.append("Lisa")
liste.remove("Stefanie")

liste=["Stefanie", "Remo", "Tamara"]
liste[0] = "Lisa"



print(liste2)
print(liste3)

for ausgabe in liste:
    print(ausgabe)


