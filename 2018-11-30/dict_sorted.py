d = {"Chur": 30000, "Zürich": 360000, "Genf": 190000}

# aufgabe a
for stadt, einw in d.items():
    print(stadt, "hat", einw, "Einwohner")

# aufgabe b - variante 1
l = list(d.keys())
l.sort()

for stadt in l:
    print(stadt, "hat", d[stadt], "Einwohner")

# aufgabe b - variante 2
l = list(d.items())
l.sort()
for stadt, einwohner in l:
    print(stadt, 'hat', einwohner, 'Einwohner.')
    
# aufgabe b - variante 3
for stadt, einwohner in sorted(d.items()):
    print(stadt, 'hat', einwohner, 'Einwohner.')
    

# aufgabe c
print("Aufgabe c----")

# leere liste
l = []
# fülle liste mit dictionary aus einwohner & stadt
# wichtig: einwohner an erster stelle, damit sort nach diesen sortiert (!)
for stadt, einwohner in d.items():
    l.append([einwohner, stadt])
    
print(l)
l.sort()
# ausgabe (einwohner und stadt sind vertauscht)
for einwohner, stadt in l:
    print(stadt, "hat", einwohner, "Einwohner")
