# Aufruf der Funktion mit
#  jump_ball(winkel, strecke, durchmesser_ball)
#
# Bewegt den Cursor im Winkel `winkel` um `strecke` Pixel und
# zeichnet dort einen Ball mit Durchmesser `durchmesser_ball` 

from turtle import *


# definition der funktion
def jump_ball(winkel, strecke, durchmesser_ball):
    lt(winkel);
    penup();
    fd(strecke);
    pendown();
    circle(durchmesser_ball / 2)
    

# definition der funktion
def jump_ball_color(winkel, strecke, durchmesser_ball, stiftfarbe, fuellung):
    lt(winkel);
    penup();
    fd(strecke);
    pendown();
    pencolor(stiftfarbe)
    fillcolor(fuellung)
    begin_fill()
    circle(durchmesser_ball / 2)
    end_fill()

# hauptprogramm

jump_ball_color(0, 30, 20, "red", "blue")

# Beispiel: Bewege den Ball im Winkel von 30° um 50 Pixel
# nach vorne und zeichne einen Ball mit Durchmesser 10
jump_ball(30, 50, 10) 

# Beispiel: Bewege den Ball im Winkel von 90° um 70 Pixel
# nach vorne und zeichne einen Ball mit Durchmesser 20
jump_ball(90, 70, 20)

a = numinput("Eingabe", "Bitte Winkel eingeben")
b = numinput("Eingabe", "Bitte Strecke eingeben")
c = numinput("Eingabe", "Bitte Durchmesser eingeben")
jump_ball(a, b, c)

