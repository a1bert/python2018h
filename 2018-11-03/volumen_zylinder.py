from math import pi


# def zylindervolumen(radius * hoehe *pi...) --> die deklaration deklariert und nicht mehr

def zylindervolumen(radius, hoehe):
    # volumen = r**2*pi*h --> r und h sind nicht definiert -> definiert sind hoehe & radius
    volumen = radius ** 2 * pi * hoehe
    return volumen  # vergessen sie niemals return, sonst gibt es kein ergebnis

def zylindervolumen2(radius, hoehe):
    return radius ** 2 * pi * hoehe

volumen = zylindervolumen(2, 2)
# volumen = zylindervolumen()  -> zylindervolumen _verlangt_ zwei parameter

print("Das Volumen beträgt: ", volumen)
# print("Das Volumen beträgt: ", volumen(2,2)) --> der befehlt heisst "zylindervolumen" und nicht "volumen"
# volumen ist die variable, die bereits berechnet worden ist....