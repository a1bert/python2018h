# notwendig, um die turtle befehle zur verfügung zu haben
from turtle import *

# zeichenfläche öffnen
reset()
forward(100)
left(120)
forward(100)
pensize(10)
pencolor("red")
left(120)
forward(100)

# für windows wichtig
exitonclick()