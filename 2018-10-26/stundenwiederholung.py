#
# Übungen zur Stundenwiederholung vom 26. Oktober 2018
#
print("Jonathan")

# funktioniert nicht, da es nicht möglich ist einen Text und eine Zahl zusammenzuzählen
#print("Die Mehrwertsteuer beträgt:" + (200 * 2.5 / 100))

# Komma bedeutet, gib jeweil den Term getrennt durch ein Lehrzeichen (Space) aus
print("Die Mehrwertsteuer beträgt:", 200 * 2.5 / 100)

# Kreisfläche
PI = 3.14
r = 2 
print(r ** 2 * PI)
print(r * r * PI)

print("Schönen Nachmittag "*10)

# Geben sie basierend auf text folgendes aus:
#  1. Donau
#  2. gesellschaft

text = "Donaudampfschifffahrtsgesellschaft"
print(text[0:5])
print(text[:5])

print(text[22:34])
print(text[22:100])

print(text[22:])
print(text[-12:])

# weitere Tricks mit Indices
print(text[::])
print(text[::-1])
print(text[::2])
print(text[1::2])
