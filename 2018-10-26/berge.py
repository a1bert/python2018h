# notwendig, um die turtle befehle zur verfügung zu haben
from turtle import *

# zeichenfläche öffnen
reset()


pensize(5)

left(45)
pencolor("blue")
fd(75)
rt(90)

pencolor("red")
fd(75)
lt(90)

pencolor("cyan")
fd(75)
rt(90)

pencolor("black")
fd(75)
rt(90)

exitonclick()
