# notwendig, um die turtle befehle zur verfügung zu haben
from turtle import *

# zeichenfläche öffnen
reset()

a = numinput("Eingabe", "Bitte die Länge des Dreiecks eingeben")

forward(a)
left(120)
forward(a)
left(120)
forward(a)
left(120)

a = a/2
forward(a)
left(120)
forward(a)
left(120)
forward(a)
left(120)
