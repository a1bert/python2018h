liste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

neueliste = {}

for lebensmittel, preis in liste.items():
    if preis > 2.5:
        neueliste[lebensmittel] = preis
    
print("Preisliste: ", neueliste)