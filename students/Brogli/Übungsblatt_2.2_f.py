liste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

for lebensmittel, preis in liste.items():
    if "en" in lebensmittel:
        print(lebensmittel, "kostet", preis, "CHF.")