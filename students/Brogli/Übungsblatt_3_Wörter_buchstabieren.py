ICAO_ALPHABET = {'A': 'Alfa',
                 'B': 'Bravo',
                 'C': 'Charlie',
                 'D': 'Delta',
                 'E': 'Echo',
                 'F': 'Foxtrot',
                 'G': 'Golf',
                 'H': 'Hotel',
                 'I': 'India',
                 'J': 'Juliett',
                 'K': 'Kilo',
                 'L': 'Lima',
                 'M': 'Mike',
                 'N': 'November',
                 'O': 'Oscar',
                 'P': 'Papa',
                 'Q': 'Quebec',
                 'R': 'Romeo',
                 'S': 'Sierra',
                 'T': 'Tango',
                 'U': 'Uniform',
                 'V': 'Victor',
                 'W': 'Whiskey',
                 'X': 'X-ray',
                 'Y': 'Yankee',
                 'Z': 'Zulu'}

#Aufgabe 0
def funktioneins(wort):
    listeeins = []
    for buchstabe in wort:
        listeeins.append(ICAO_ALPHABET[buchstabe])
    ergebniseins = "-".join(listeeins)
    return ergebniseins

#Aufgabe a
def funktionzwei(wort):
    wort = wort.upper()
    listezwei = []
    for buchstabe in wort:
        if buchstabe not in ICAO_ALPHABET:
            print("Kann " + wort + " nicht buchstabieren, da '" + buchstabe + "' nicht definiert wurde.")
            return

        listezwei.append(ICAO_ALPHABET[buchstabe])
    ergebniszwei = "-".join(listezwei)
    return ergebniszwei

#Aufgabe b

def funktiondrei(wort):
    wort = wort.upper()
    wort = wort.replace("Ä", "AE").replace("Ö", "OE").replace("Ü", "UE")
    return funktionzwei(wort)

#Hauptprogramm

wort = input("Welches Wort soll ich buchstabieren: ")
ausgabe = funktiondrei(wort)
if ausgabe:
    print ("Ausgabe:", ausgabe)






