def funktion(text):
    statistik = {}
    
    kleinertext = text.lower()
    liste = kleinertext.split()
    
    for wort in liste:
        if wort in statistik:
            statistik[wort] = statistik[wort] + 1
        else:
            statistik[wort] = 1

    return statistik

input = "Schöner schöner schöner morgen morgen heute."
print(funktion(input))
    