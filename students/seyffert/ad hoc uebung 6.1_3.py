#Aufgabe 3:
        
try:
    with open("text.txt", "r") as source:
        with open("text2.txt", "w") as copy:
            nr = 0
            for line in source:
                nr = nr + 1
                copy.write(str(nr) + ": " + line)

except FileNotFoundError:
    print("File is missing")
except PermissionError:
    print("You are not allowed to read this file")


