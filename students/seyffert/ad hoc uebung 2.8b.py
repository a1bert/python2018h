zahlen = [1, 2, 3, 4, 5, 6, 7]
print("Die Zahlenreihe: " + str(zahlen)[1:-1])

for nr in zahlen:
    minimum = min(zahlen)
    maximum = max(zahlen)
    durchschnitt = sum(zahlen) / 7

print("Das Minimum ist: " + str(minimum))
print("Das Maximum ist: " + str(maximum))
print("Der Durchschnitt ist: " + str(durchschnitt))
