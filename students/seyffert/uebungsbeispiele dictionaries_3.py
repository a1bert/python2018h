d = {'A': 'Alfa',
                     'B': 'Bravo',
                     'C': 'Charlie',
                     'D': 'Delta',
                     'E': 'Echo',
                     'F': 'Foxtrot',
                     'G': 'Golf',
                     'H': 'Hotel',
                     'I': 'India',
                     'J': 'Juliett',
                     'K': 'Kilo',
                     'L': 'Lima',
                     'M': 'Mike',
                     'N': 'November',
                     'O': 'Oscar',
                     'P': 'Papa',
                     'Q': 'Quebec',
                     'R': 'Romeo',
                     'S': 'Sierra',
                     'T': 'Tango',
                     'U': 'Uniform',
                     'V': 'Victor',
                     'W': 'Whiskey',
                     'X': 'X-ray',
                     'Y': 'Yankee',
                     'Z': 'Zulu'}

x = input("Welches Wort soll ich buchstabieren? ")

def buchstabieren(eingabewort):
    eingabewort = eingabewort.upper() #alles wird gross geschrieben
    eingabewort = eingabewort.replace("Ä", "AE").replace("Ö", "OE").replace("Ü", "UE") #umlaute ersetzen
    
    l = [] #ergebnisliste
    for bst in eingabewort:
        l.append(d[bst])
        
    #damit mit - zwischen wörtern ausgegeben wird:
    ausgabe = "-".join(l)
    return ausgabe

print(buchstabieren(x))
