#schleife für reihe
for nr1 in range(1, 11):
    #abstand zwischen reihen
    print() 
    print("----------")
    print()
    
    print(str(nr1) + "er Reihe")
    
    #schleife für reihe mal 1 bis 10
    for nr2 in range(1, 11):
        print(nr2, "x", nr1, "=", nr2 * nr1)
