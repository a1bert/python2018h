#a liste erstellen
preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

#b preis für milch auf 2.05
preisliste["Milch"] = 2.05

#c eintrag für brot entfernen
del preisliste["Brot"]

#d neue einträge hinzufügen
preisliste ["Tee"] = 4.2
preisliste ["Peanuts"] = 3.9
preisliste ["Ketchup"] = 2.1

print(preisliste)