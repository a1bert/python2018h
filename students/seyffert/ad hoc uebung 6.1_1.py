# Aufgabe 1:
with open("text.txt", "r") as source:
    with open("text2.txt", "w") as copy:
        for line in source:
            copy.write(line)
            
# Variante. Dateiinhalt in Variable "data" einlesen, dann Inhalt der Variable "data" in neue Datei schreiben.
with open("text.txt", "r") as source:
    data = source.read() #lese den kompletten inhalt von text.txt in data ein

with open("text.txt", "w") as dest:
         dest.write(data) 
          