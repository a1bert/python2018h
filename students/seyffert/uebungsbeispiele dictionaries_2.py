x = input("Schreibe einen Satz: ")
x = x.lower()
x = x.split()

def word_stat(text):
    #leere liste für zählung der wörter
    zaehlung = {}
    
    for word in text:
        if word in zaehlung:
            zaehlung[word] = zaehlung[word] + 1
        else:
            zaehlung[word] = 1
            
    return zaehlung
        
print(word_stat(x))