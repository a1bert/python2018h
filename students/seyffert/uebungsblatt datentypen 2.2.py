#Liste aus Aufgabe 2.1
# a
preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

# b
preisliste["Milch"] = 2.05

# c
del preisliste["Brot"]

# d
preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1

#Aufgabe 2.2
print("a) Preisliste einfach ausgeben")
for item, preis in preisliste.items():
    print(item, "kostet", preis, "CHF")

print()
print("b) Preisliste alphabetisch sortiert ausgeben")

l = list(preisliste.items())
l.sort()
for item, preis in l:
    print(item, "kostet:", preis, "CHF")
 
print()
print("c) Preisliste nach Preis sortiert ausgeben")

l = []
for item, preis in preisliste.items():
    l.append([preis, item])

l.sort()
for preis, item in l:
    print(item, "kostet", preis, "CHF")

print()
print("d) Preisliste absteigend nach Preis sortiert ausgeben")
l.reverse()
for preis, item in l:
    print(item, "kostet", preis, "CHF")
    
print()
print("e) nur Lebensmittel mit Preis < 2 ausgeben")

for preis, item in l:
    if preis < 2:
        print(item, "kostet", preis, "CHF")

print("ergibt nichts, weil kein Item einen Preis unter 2 hat")

print()
print("f) nur Lebensmittel ausgeben, in denen 'en' vorkommt")

for preis, item in l:
    if "en" in item:
        print(item, "kostet", preis, "CHF")


