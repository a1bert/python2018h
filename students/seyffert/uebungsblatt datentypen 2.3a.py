#liste erstellen
preisliste = {"Brot": 3.2, "Milch": 2.05, "Orangen": 3.75, "Tomaten": 2.2, "Tee": 4.2, "Peanuts": 3.9, "Ketchup": 2.1}
hochpreisliste = {}

print("a) Lebensmittel unter 2.5 CHF aus Dictionary entfernen")

for item, preis in preisliste.items():
    if preis > 2.5:
        hochpreisliste[item] = preis

print(hochpreisliste)

#Variante
l = list(preisliste.items())

for item, preis in l:
    if preis > 2.5:
        print(item, "kostet:", preis, "CHF")