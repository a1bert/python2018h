dict = {"Hund": "dog", "Katze": "cat", "Haus": "house", "Zug": "train", "Auto": "car", "Fehler": "error", "Schwester": "sister",\
        "Bruder": "brother", "Garten": "garden"}
versuche = -1

for de, en in dict.items():
    print("Was heisst", de, end=" ")
    eingabe = input()
    versuche = versuche + 1
    
    if eingabe == en:
        print("RICHTIG")
    elif eingabe != en:
        print("FALSCH!")
        
print("Du hast " + str(versuche) + " von 10 Vokabeln richtig beantwortet.")
        
    
    