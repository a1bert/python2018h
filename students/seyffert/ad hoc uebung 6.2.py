#1. URL entgegennehmen
url = input("Gib eine URL an: ")

def readurl():
    from urllib.request import urlopen

    with urlopen(url) as src:
        #aus Data Wortliste erstellen
        data = src.read().decode("utf-8")
    
    wortliste=data.split()
    #mit len Anzahl Elemente der Sequenz zählen
    print(len(wortliste))

try:
    readurl()

except:
    url = input("URL konnte nicht geöffnet werden. Gib noch einmal eine an: ")
    readurl()
