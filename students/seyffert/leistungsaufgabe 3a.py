print("Stadt und Einwohnerzahl angeben oder Programm beenden mit x")
Dictionary = {}
eingabe = 0

while eingabe != "x":
    stadt = input("Gib die Stadt ein: ")
    if stadt == "x":
        break
    
    einwohner = float(input("Gib die zugehörige Einwohnerzahl ein: ")) #ohne float würde es als string gespeichert
    Dictionary[stadt] = einwohner
    
    l = list(Dictionary.items())
    l.sort()
    
print(l)