from turtle import *
reset()

#a)

def dreieck():
    
    fillcolor("blue")
    begin_fill()
    
    for seite in range(3):
        pencolor("blue")
        fd(50)
        left(120)
        
    end_fill()
    penup()
    fd(100)

anzahl = 0
while anzahl <= 4:
    dreieck()
    
    anzahl = anzahl + 1

exitonclick()
