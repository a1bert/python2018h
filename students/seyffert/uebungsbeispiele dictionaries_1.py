x = input("Schreibe ein Wort oder einen Satz: ")

def letter_stat(text):
    #leere liste für zählung der buchstaben
    zaehlung = {}
    
    for bst in text:
        if bst in zaehlung:
            zaehlung[bst] = zaehlung[bst] + 1
        else:
            zaehlung[bst] = 1
            
    return zaehlung
        
print(letter_stat(x))