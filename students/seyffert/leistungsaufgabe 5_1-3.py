#remote ressourcen als dateien zur verfügung stellen
from urllib.request import urlopen

x = input("Gib Textdatei oder URL an: ")

try:
    with open(x) as src:
        data = src.read()

except OSError:
    with urlopen(x) as src:
        data = src.read().decode("utf-8")

    data = data.lower()
          
#buchstaben zählen
zaehlung2 = {}
    
for bst in data:
    if bst in zaehlung2:
        zaehlung2[bst] = zaehlung2[bst] + 1
    else:
        zaehlung2[bst] = 1

#wörter zählen
wortliste=data.split()
zaehlung1 = {}
    
for wort in wortliste:
    if wort in zaehlung1:
        zaehlung1[wort] = zaehlung1[wort] + 1
    else:
        zaehlung1[wort] = 1

print()
print("Wörter")
print()
lwort = list(zaehlung1.items())
lwort.sort()

for wort, anzahl in lwort:
    print(str(wort) + ": " + str(anzahl))

print()
print("Buchstaben")
print()
lbst = list(zaehlung2.items())
lbst.sort()

for buchstabe, anzahl in lbst:
    print(str(buchstabe) + ": " + str(anzahl))

#wortliste in csv datei schreiben
from csv import writer
with open('wort.csv', 'w') as datawort:
    csv_writer = writer(datawort)
    for row in lwort:
        csv_writer.writerow(row)
    
#buchstabenliste in csv datei schreiben        
from csv import writer
with open('buchstabe.csv', 'w') as databuchstabe:
    csv_writer = writer(databuchstabe)
    for row in lbst:
        csv_writer.writerow(row)
