#Uebung 6.1 1-3

i=0

try:                                       #für Fehlermeldungen

    with open ("url open.py", "r") as source: #öffnet das ZU SPEICHERNDE Dokument
        with open("url open_sicherung.py", "w") as save: #kreiert und öffnet das Zieldokument
            for zeile in source:    #iteriert über alle Zeilen des Dokuments
                zeile=str(i)+":"+zeile  #nummeriert jede Zeile und macht ein Doppelpunkt
                                    #WARUM wird hier das "+zeile" gebraucht??
                
                save.write(zeile)  #speichert(schreibt) jede Zeile nach der Nummerierung
                i=i+1              #erhöht die Nummerierung pro Zeile um 1
                
except FileNotFoundError:          #gehört zum try am Anfang des Codes. ändert Text bei Fehlermeldungen ab.
    print("Das Dokument konnte nicht gefunden werden.")
except PermissionError:
    print("Sie haben keine Rechte das Dokument zu öffnen.")