#Variante 1:

x=[1,2,3,4,5,6,7,8,9,10]

def einmaleins():
    
    multiplikator = 1
    
    for reihenname in x:                   #gibt 10x eine Reihe von 1-10 aus (in der Liste x definiert)
        print(str(reihenname)+"er Reihe:")
                     
        for zahl1 in x:                    #gibt eine Reihe aus von 1-10 (in der Liste x definiert)
            ergebnis = zahl1*multiplikator
            print(str(zahl1) + " x " +str(multiplikator) +" = " + str(ergebnis))
            
        multiplikator = multiplikator + 1  #der Multiplikator jeder neuen Schleife erhöht sich um 1
        print("___________")

einmaleins()

print("____________________________________________________________________________________________")

#Variante 2:



def einmaleins():
    
    multiplikator = 1
    
    for reihenname in range(1,11):            #gibt die Reihe 10 mal aus von 1-10
        print(str(reihenname)+"er Reihe:")
                     
        for zahl1 in range (1,11):            #gibt eine Reihe von 1-10 aus
            ergebnis = zahl1*multiplikator
            print(str(zahl1) + " x " +str(multiplikator) +" = " + str(ergebnis))
            
        multiplikator = multiplikator + 1     #der Multiplikator wird um 1 erhöht
        print("___________")

einmaleins()

    
