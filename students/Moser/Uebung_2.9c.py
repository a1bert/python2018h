from turtle import *
reset()

ad = numinput("Eingabe", "Gewünschte Anzahl Dreiecke pro Reihe")
ar = numinput("Eingabe", "Gewünschte Anzahl Reihen")

def dreieck(sl):
    for reihe in range (int(ar)):
        
        if reihe >= 1:    #Wieso funktioniert das mit ==1 nicht?
                penup()
                backward ((ad*sl)+(ad*(sl*0.5)))
                right(90)
                fd(sl*1.5)
                left(90)
                pendown()
            
        for anzahldreiecke in range (int(ad)):
            fillcolor("blue")  
            begin_fill()
            forward(sl)
            left(120)
            forward(sl)
            left(120)
            forward(sl)
            left(120)
            penup() #fährt noch an Startpunkt des nächsten Dreiecks
            fd(sl+sl*0.5)
            pendown()
            end_fill()
            
    
        
dreieck(80)

hideturtle()
exitonclick()

    



