# ad hoc 2.6 c lotto
# 6 zufallszahlen zw. 1 und 42 ausgeben. Ausgabe wie folgt: 1. Zufallszahl: ... , ... , 6. Zufallszahl: ...
# hinweis: zufallszahlen --> from random import randint --> zufallszahl = randinrt(kleinster_wert, höchster_wert)

from random import randint

for i in range(1, 7):
    zufallszahl = randint(1, 42)
    print( i, ". Zufallszahl: ", zufallszahl)
