#2.9
#a
##from turtle import *
##
##reset()
##
##def dreieck():
##    pu() ; goto(-250, 100) ; pd()
##    fillcolor("blue")
##    begin_fill()
##    for i in range(4):
##        fd(100) ; lt(120) ; fd(100) ; lt(120) ; fd(100) ; lt(120) ; fd(100) ;
##        pu() ; fd(20) ; pd()
##    end_fill()
##    
##dreieck = dreieck()
##
##exitonclick()

#b
##from turtle import *
##
##reset()
##
##def dreieck():
##    pu() ; goto(-250, 100) ; pd()
##    anzahl = int(numinput("Anzahl Dreiecke", "Gewünschte Anzahl der gezeichneten Dreiecke: "))
##    fillcolor("blue")
##    begin_fill()
##    for i in range(anzahl):
##        fd(100) ; lt(120) ; fd(100) ; lt(120) ; fd(100) ; lt(120) ; fd(100) ;
##        pu() ; fd(20) ; pd()
##    end_fill()
##    
##dreieck = dreieck()
##
##exitonclick()

#c
from turtle import *

reset()

def dreieck():
    pu() ; goto(-250, 100) ; pd()
#wieso funktioniert numinput ohne das int() zuvor nicht? zeigt mir fehler an, vonwegen float und integer.. ist numinput nicht automatisch integer?   
    anzahl_dreiecke = int(numinput("Anzahl Dreiecke", "Gewünschte Anzahl der gezeichneten Dreiecke: "))
    anzahl_reihen = numinput("Anzahl Reihen", "Anzahl von Dreiecks-Reihen: ")
    fillcolor("blue")

    while anzahl_reihen != 0:
        for i in range(anzahl_dreiecke):
            begin_fill()
            fd(100) ; lt(120) ; fd(100) ; lt(120) ; fd(100) ; lt(120) ; fd(100) ;
            pu() ; fd(20) ; pd()
            end_fill()
        pu() ; rt(90) ; fd(120) ; rt(90) ; fd(120 * anzahl_dreiecke) ; rt(180) ; pd()
        anzahl_reihen = anzahl_reihen - 1
    
dreieck = dreieck()

exitonclick()
