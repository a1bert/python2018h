# übungsbeispiele funktionen ,... 11 a + e

#11a    
##for i in range(1, 9):
##    if i < 9:
##        print("# " * i)
##        i = i + 1
##        
#11e

rahmen = "# "*7
links = rahmen[0:1]
rechts = rahmen[11:12]

for i in range(1, 8):
    if i == 1 or i == 7:
        print(rahmen)
    else:
        print(links, rechts)

