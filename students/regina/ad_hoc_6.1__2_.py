# 6.1, 2) Programm so verändern, dass in der Zieldatei bei jeder Zeile zuerst die Zeilennummer gefolgt von einem Doppelpunkt steht.

# Mein 1. Versuch
##x = 0
##with open("data.py", "r") as source:
##    data = source.read()
##    
##    with open("data_copy3.py", "w") as save:
##        for line in data:
##            print(x + 1, ":")
##            save.write(line)          
       
i = 1
with open("data.py", "r") as source:
    with open("data_copy.py" , "w") as save:
        for line in source:
            line = str(i) + ": " + line
            save.write(line)
            i = i + 1

## Version Pascal:       
##with open("data.py", "r") as source:
##    data = source.read()
##
##    with open("data_copy5.py", "w") as f:
##        for i in range():
##            print(i, ":")
##        f.write(data)
