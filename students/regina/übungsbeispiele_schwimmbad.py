# schwimmbadbeispiel 

def berechnet_eintrittspreis(karte, rabatt, alter):
    preis = 0
    if karte == "kurzzeitkarte":
        preis = 5
    elif karte == "nachmittagskarte":
        preis = 6
    elif karte == "tageskarte":
        preis = 10
    
    if alter <= 6:
        preis = 0
    elif alter <= 12:
        preis = preis / 2
        
    if rabatt:
        preis = preis * 0.9
        
    return preis

print("Preis: ", berechnet_eintrittspreis("kurzzeitkarte", False, 30))
print("Preis: ", berechnet_eintrittspreis("tageskarte", False, 2))
print("Preis: ", berechnet_eintrittspreis("tageskarte", True, 12))
