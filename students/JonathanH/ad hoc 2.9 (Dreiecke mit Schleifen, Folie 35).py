# ad hoc Übung 2.9 (Dreiecke mit verschachtelten Schleifen) (Folie 35)

from turtle import*
reset()

# Funktion Dreieck
def dreieck(ffarbe, stfarbe, stdicke, s, winkel):
    fillcolor(ffarbe)
    begin_fill()
    for count in range(3):
        fd(s)
        left(winkel)
    end_fill()
    
    penup()
    fd(s + s/3)
    pendown()

# Das Dreieck 4mal hintereinander zeichnen

for count in range(4):
    dreieck("steelblue", "blue", 2, 100, 120)


hideturtle()

exitonclick()