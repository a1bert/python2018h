# Übungsbeispiele zu Funktionen u. Kontrollstrukturen
# Beispiel 4 (Schwimmbad)

#Eingabemöglichkeiten:
kar=int(input("Welche Karte? (Tageskarte = 2 / Nachmittagskarte = 3 / Kurzzeitkarte = 4)"))
alt=int(input("Wie alt? "))
rab=int(input("Ermaessigungskarte? (ja = 1 / nein = 0)"))

# Funktion zur Berechnung des Billetpreises
def preis_karte(karte, alter, rabatt):
    
    # Die verschiedenen karten in ihren Grundpreisen (ohne rabatt, für erwachsene)
    if karte==2:
        preis=10
    
    elif karte==3:
         preis=6
        
    elif karte==4:
         preis=5
         
    else:
        preis=0
    
    # Die Position "alter":
    if alter <=6:
        preis=0
    
    elif alter <=12:
        preis=preis/2
    
    else:
        preis=preis
    
    # rabatte
    if rabatt==1:
         preis=preis + (preis*0.1)
         
    elif rabatt==0:
        preis=preis
    
    return preis

print(preis_karte(kar, alt, rab))
    
    


    
