# Übungsbeispiele zu Funktionen u. Kontrollstrukturen
# Beispiel 11e (Spass mit Schleifen)

# a)
i = 1
while i <= 8:
        print("# " * i)
        i = i + 1
print()


# e)
i = 1
while i <=7:
    if i == 1 or i == 7:
        print("# " * 7)
    else:
        print("# " + "  " * 5 + "#")
    i = i + 1
print()



