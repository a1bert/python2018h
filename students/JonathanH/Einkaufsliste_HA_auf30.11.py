#Einkaufsliste schreiben (mit Menü-Funktionen)

#leere Liste zum starten
eliste=[]

# Menüpunkt 1 Eingabe (mittels Schleife nach item (input) fragen (solange item nicht x ist))
def items_eingeben():
    item = 0  # item muss nun innerhalb der Funktion definiert sein, sonst Fehlermeldung
    while item != "x":
        item=input("Item eingeben: ")
    
        # Bedingungen festlegen:
        #wenn eingabe(also das item) nicht x ist, dann soll ein weiteres item in Liste aufgenommen werden
        if item !="x":
            eliste.append(item)
        
        #Wenn item aber == x ist, dann Abbruch
        # und erneutes Anzeigen des Hauptmenüs und der Auswahlmöglichkeiten
        # d.h. also aufrufen der funktion hauptmenue()
        elif item == "x":
            hauptmenue()
        
        
# Menüpunkt 2 items korrigieren
def items_korrigieren():
    korrektur=input("Korrigieren (k) oder löschen (l)? ")
    
    # ändern
    if korrektur == "k":
        # mit enumerate() elemente in liste numerieren
        for item in enumerate(eliste):
            print(item)
        eingabek_nr=int(input("item nr wählen: "))
        eingabe_korr=input("Bitte item korrigieren. ")
        eliste[eingabek_nr] = eingabe_korr
            
        hauptmenue()
    
    #löschen
    elif korrektur == "l":
        # mit enumerate() elemente in liste numerieren
        for item in enumerate(eliste):
            print(item)
        eingabel_nr=int(input("item nr wählen: "))
        del eliste[eingabel_nr]
        
        hauptmenue()
    
    else:
        print("Falsche Eingabe: Korrigieren (k) oder löschen (l)? ")
        items_korrigieren()
        
        
# Menüpunkt 3 items ausgeben
def items_ausgeben():
    #items sortiert ausgeben
    eliste.sort()
    print("Einkaufsliste: ")
    for item in eliste:
        print(item)
    
    hauptmenue()


# Menüpunkt 4: Programm verlassen
def programm_verlassen():
    print("Programm beendet!")

    
    
# Es braucht ein Hauptmenü (das alle Menü-(Unter)punkte aufzeigt); muss am Ende stehen,
#da sonst die einzelnen Untermenüs also die Funktionen (z.b. items_eingeben) nicht definiert sind
def hauptmenue():
    print("""
    Menü
    -------
    1 Items eingeben
    2 Items korrigieren
    3 Items ausgeben
    4 Programm verlassen
    """)
    
    # das in Kombination mit Eingabemöglichkeit, das die jeweiligen Funktionen aufruft
    eingabe=int(input("Menü wählen: "))
    if eingabe == 1:
        items_eingeben()
    
    elif eingabe == 2:
        items_korrigieren()
        
    elif eingabe == 3:
        items_ausgeben()
        
    elif eingabe ==4:
        programm_verlassen()
        
    else:
        print("Falsche Eingabe, drücke 1, 2, 3 oder 4.")
        hauptmenue()
        
hauptmenue()   
        