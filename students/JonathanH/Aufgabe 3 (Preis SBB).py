# "Übungsbeispiele - Funktionen und Kontrollstrukturen"
# Schwerpunkt Funktionen und Bedingungen
# Aufgabe 3

# Es braucht zwei Eingaben, die abgefragt werden müssen: Alter und Entfernung
alter=int(input("Bitte Alter angeben: "))
entfernung=float(input("Entfernung in km angeben: "))

# Funktion zur Berechnung des Billetpreises -> alter (a), entfernung (e)
def billet_preis(a, e):
    grundpreis=2 + (0.25*e)
    
    if a<16:
         preis=grundpreis/2
    
    elif a<6:
         preis=0
    
    else:
        preis=grundpreis
    
    return preis

print(billet_preis(alter, entfernung))
