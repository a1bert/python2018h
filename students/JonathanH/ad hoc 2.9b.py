# ad hoc Übung 2.9 b) (Dreiecke mit verschachtelten Schleifen, mit Benutzereingabe Anzahl Dreiecke)
#(Folie 35)

from turtle import*
reset()

#Benutzereingabe Anzahl Dreiecke
anzahl=numinput("Eingabe:", "Wieviele Dreiecke? ")

# Funktion Dreieck
def dreieck(ffarbe, stfarbe, stdicke, s, winkel):
    fillcolor(ffarbe)
    begin_fill()
    for count in range(3):
        fd(s)
        left(winkel)
    end_fill()
    
    penup()
    fd(s + s/3)
    pendown()

# Das Dreieck xmal hintereinander zeichnen (je nach benutzer-input)
for count in range(int(anzahl)):
    dreieck("steelblue", "blue", 2, 100, 120)


hideturtle()

exitonclick()