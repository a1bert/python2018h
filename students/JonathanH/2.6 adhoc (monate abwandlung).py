#ad hoc Übung 2.6 (Folie 24)
# Es funktioniert, aber irgendwas stimmt noch nicht ganz (Eingabemöglichkeiten nicht ganz richtig.) :-(

#Eingabemöglichkeit:
monateingabe=int(input("Bitte eine Zahl eingeben: "))

#Funktion für Monate
def ausgabe_monat(m):
    # Bedingung für Zahlen, die keine Monate sind
    if m < 1 or m > 12:
        ausgabe = int(input("Bitte eine Zahl eingeben zwischen 1 und 12: "))
         
    # Für die Monate:  
    if m==1:
       ausgabe="Januar"
    
    elif m==2:
         ausgabe="Februar"

    elif m==3:
         ausgabe="März"
         
    elif m==4:
         ausgabe="April"
         
    elif m==5:
         ausgabe="Mai"
         
    elif m==6:
         ausgabe="Juni"
         
    elif m==7:
         ausgabe="Juli"
         
    elif m==8:
         ausgabe="August"
         
    elif m==9:
         ausgabe="September"
         
    elif m==10:
         ausgabe="Oktober"
         
    elif m==11:
         ausgabe="November"
         
    elif m==12:
         ausgabe="Dezember"
         
    return ausgabe

# Hauptprogramm; aufrufen der Funktion
print(ausgabe_monat(monateingabe))

# Nun braucht es noch die Schleife, damit es sich wiederholt
while monateingabe !=99:
     monateingabe=int(input("Bitte eine Zahl zwischen 1 und 12 eingeben oder mit 99 beenden. "))
     
     if monateingabe==99:
         print("Programmende!!!!")
    
     else:
         print(ausgabe_monat(monateingabe))
     