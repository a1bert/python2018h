# Leistungsaufgabe 3 b) (deutsch-englisch wörterbuch)

#deutsch-engl-dictionary erstellen mit 10 darin enthaltenen Vokabeln
dict_d_engl = {"Haus":"house", "Hund":"dog", "Katze":"cat", "Vogel":"bird", "Baum":"tree", "Sonne":"sun", "Regen":"rain", "grün":"green", "blau":"blue", "rot":"red"}

#Abfragestatistik-> zähler (siehe "Python für Kids", S. 67ff.)
richtige=0

for deutsch in dict_d_engl:
    print("Was heisst: ", deutsch)
    eingabe=input()
    
    
    if eingabe in dict_d_engl.values():
        print("RICHTIG!")
        richtige = richtige + 1
        
    else:
         print("FALSCH!")

print("Sie haben", str(richtige), "von 10 Vokabeln richtig beantwortet!")
    