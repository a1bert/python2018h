# Übungsblatt Datentypen 2 (Dictionaries)
#2.3 a (lebensmittel filtern)

# --> IRGENDWAS FUNKTIONIERT NOCH NICHT:
# Es gibt mir zwar das korrekte Ergebnis aus, aber gleich gefolgt von
# Fehlermeldung (RuntimeError: dictionary changed size during interation)

# Von Aufgabe 2.1 (e,f; lebensmittel und preise) Eingabemöglichkeit übernehmen

# leeren dictionary erstellen
d = {}

# Eingabemöglichkeiten und aufnahme in dictionary
lebensmittel = ""

while lebensmittel != "x":
     lebensmittel=input("Lebenmittel: ")
     if lebensmittel =="x":
         # braucht abbruch (mit break) und muss in dieser Schleife, die nach lebensmittel fragt stehen,
         # weil sonst wird nach der Eingabe von x noch nach dem Preis gefragt,
         # und in der Ausgbae x (als "x" : "x") ausgegeben
         break
        
    # Preis-Eingabe (als Zahl mit float (->Kommazahlen) davor)
     preis=float(input("Preis: "))
     
     # hinzufügen in dictionary
     d[lebensmittel] = preis
     

# 2.a) Alle Lebensmittel aus dictionary entfernen, die weniger als 2.5 kosten (preis<2.5)
for lebensmittel, preis in d.items():
    if preis < 2.5:
        del d[lebensmittel]
    
        print("Nur Lebensmittel unter 2.5: ", lebensmittel, preis)