# Leistungsaufgabe 3 a) (städte und einwohner)

# leeren dictionary erstellen
d = {}

# Eingabemöglichkeiten und aufnahme in dictionary
stadt = ""

while stadt != "x":
     stadt=input("Geben Sie die Stadt ein: ")
     if stadt =="x":
         break
        
    # Einwohner-Eingabe
     einwohner=input("Geben Sie die zugehörige Einwohnerzahl ein: ")
     
     # hinzufügen in dictionary
     d[stadt] = einwohner

# leere Liste erstellen
liste= []

# liste füllen
for stadt, einwohner in d.items():
    liste.append([stadt, einwohner])
    
#Ausgabe alphabetisch sortiert
liste.sort()
print (liste)
