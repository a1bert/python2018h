# Übungsblatt Datentypen 2 (Dictionaries)
#2.1 e, f (lebensmittel und preise)

# leeren dictionary erstellen
d = {}

# Eingabemöglichkeiten und aufnahme in dictionary
lebensmittel = ""

while lebensmittel != "x":
     lebensmittel=input("Lebenmittel: ")
     if lebensmittel =="x":
         # braucht abbruch (mit break) und muss in dieser Schleife, die nach lebensmittel fragt stehen,
         # weil sonst wird nach der Eingabe von x noch nach dem Preis gefragt,
         # und in der Ausgbae x (als "x" : "x") ausgegeben
         break
        
    # Preis-Eingabe (als Zahl mit float (->Kommazahlen) davor)
     preis=float(input("Preis: "))
     
     # hinzufügen in dictionary
     d[lebensmittel] = preis
     

print("Dictionary: ", d)
