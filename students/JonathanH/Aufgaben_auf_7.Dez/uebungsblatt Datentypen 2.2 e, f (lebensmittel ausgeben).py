# Übungsblatt Datentypen 2 (Dictionaries)
#2.2 e, f (lebensmittel ausgeben)

# Von Aufgabe 2.1 (e,f; lebensmittel und preise) Eingabemöglichkeit übernehmen

# leeren dictionary erstellen
d = {}

# Eingabemöglichkeiten und aufnahme in dictionary
lebensmittel = ""

while lebensmittel != "x":
     lebensmittel=input("Lebenmittel: ")
     if lebensmittel =="x":
         # braucht abbruch (mit break) und muss in dieser Schleife, die nach lebensmittel fragt stehen,
         # weil sonst wird nach der Eingabe von x noch nach dem Preis gefragt,
         # und in der Ausgbae x (als "x" : "x") ausgegeben
         break
        
    # Preis-Eingabe (als Zahl mit float (->Kommazahlen) davor)
     preis=float(input("Preis: "))
     
     # hinzufügen in dictionary
     d[lebensmittel] = preis
     
#Ausgabe anpassen:
# e) Nur Lebensmittel mit Preis < 2.0 ausgeben,
#d.items() weil Schlüssel und Werte erzeugt werden sollen
for lebensmittel, preis in d.items():
    if preis < 2:
        print("Preis kleiner als 2:", lebensmittel, preis)
        
#Darf nicht eingerückt sein, weil sonst ist es in Schleife und wird so oft ausgegeben, wie es eingaben hat.    
else:
    print("Sorry, kostet mehr als 2.")


# f) Alle Lebensmittel ausgeben, in denen Zeichenkette "en" vorkommt
for lebensmittel, preis in d.items():
    if "en" in lebensmittel:
        print("Zeichenkette <en> enthalten: ", lebensmittel, preis)
    
else:
    print("Leider keine Zeichenkette <en> enthalten.")
        