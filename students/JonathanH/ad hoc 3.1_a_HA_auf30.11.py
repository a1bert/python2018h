# ad hoc Übung 3.1 a) Zeichenkette einlesen und anschliessend Ausgabe der Zeichenkette,
#bestehend nur noch aus *****.

#Eingabemöglichkeit
eingabe=input("Text eingeben: ")

# Zeichenkette in Sterne "umwandeln", len liefert Anzahl Elemente einer Sequenz
sterne = eingabe.replace(eingabe, "*" * len(eingabe))

print(sterne)