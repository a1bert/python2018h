# ad hoc 6.1

# 1. Programm schreiben, das Sicherungskopie einer Datei erstellt
# Diese Datei muss bereits bestehen, sonst geht es nicht.
# Achtung:: Pfad muss stimmen -> zu öffnende Datei im gleichen Ordner wie Python-Programm

with open ("datei_text.txt") as source:

    #schreiben und öffnen der neuen Datei, der Sicherungskopie
    with open ("datei_text_kopie.txt", "w") as copy:
        for line in source:
            copy.write(line)
            
            print(line)



