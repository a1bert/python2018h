# ad hoc 6.1. 3) Abfangen von Fehlern

# Abfangen von Fehler (zwischen try und except)
#Datei kann nicht geöffnet werden
try:
    with open ("datei_text.txt") as source:
        n = 0
        with open ("datei_text_kopie.txt", "w") as copy:
            for line in source:
                n = n + 1
                line = str(n) + ": " + line
                copy.write(line)
            
                #print(line)

except FileNotFoundError:
    print("Datei konnte nicht geöffnet werden, File is missing")

except PermissionError:
    print("Datei konnte nicht geschrieben werden, permission error")
    