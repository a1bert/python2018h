# Leistungsaufgabe 5 a) (Wortstatistiken) --> klappt noch nicht ganz alles wie es sollte :-(

# urlopen und writer für csv importieren (sonst funktioniert es nicht)
from urllib.request import urlopen
from csv import writer

#weiss nicht, ob das der richtige Ort ist; aber ohne leeres dict am Anfang,
#funktioniert es unten beim csv schreiben nicht...
anzahl={}

#Eingabemöglichkeit (bzw. Entgegennahme der URL oder Name des CSV Files)
eingabe = input("Bitte URL eingeben (z.B. http://www.xyz.ch) oder Dateiname (z.B: datei_text.txt): ")

#Wenn Eingabe = URL
if eingabe[0:7] == "http://":
    with urlopen (eingabe) as source:
        web_content = source.read().decode("utf8")
        # div. "Vorbereitungen" damits richtig zählt: alles klein mit .lower();
        # Satzzeichen, Zahlen, Sonderzeichen entfernen mit .replace
        # irgendwie klappt das aber beim zählen dann noch nicht so recht;
        #(vermutlich nicht alles an Zeichen erwischt...)
        # -> Gibt es schnellere und sicherere (damit kein Zeichen vergessen geht) Variante,
        #als alle einzeln mit .replace() aufzuzählen?
        web_content = web_content.lower()
        web_content = web_content.replace(",", "").replace("!", "").replace("?", "").replace(".", "").replace(";", "").replace(":", "").replace("(", "").replace(")", "").replace("-", "").replace("0","").replace("1","").replace("2","").replace("3","").replace("4","").replace("5","").replace("6","").replace("7","").replace("8","").replace("9","")
        
        # (Gesamt-)Anzahl Worte zählen mit len()
        def worte_anzahl(text):
            text=web_content.split()
            zaehlen=len(text)
            
            return zaehlen
        
        # Anzahl Vorkommen der Worte -> mit .split() text in wortliste umwandeln
        #alphabetisch geordnet und untereinander gelistet wäre noch schön, klappt beides noch nicht
        def wortvorkommen(text):
            anzahl={}
            text=web_content.split()
            
            for wort in text:
                if wort in anzahl:
                    anzahl[wort] = anzahl[wort] + 1
                
                else:
                    anzahl[wort] = 1
            
            return anzahl
        
        # Anzahl Vorkommen der Buchstaben -> .split() darf hier nicht rein wirken, sonst klappt es nicht
        #alphabetisch geordnet und untereinander gelistet wäre noch schön, klappt noch nicht :-(
        def buchstabenvorkommen(text):
            anzahl={}
            text=web_content
            
            for buchstabe in text:
                if buchstabe in anzahl:
                    anzahl[buchstabe] = anzahl[buchstabe] + 1
                
                else:
                    anzahl[buchstabe] = 1
            
            return anzahl
        

#Eingabe ist Dateiname (holt Daten aus einer Datei)
else:
    #Hier passiert eigentlich (fast) dasselbe wie oben
    with open (eingabe) as source:
        file_content=source.read()
        file_content = file_content.lower()
        file_content = file_content.replace(",", "").replace("!", "").replace("?", "").replace(".", "").replace(";", "").replace(":", "").replace("(", "").replace(")", "").replace("-", "").replace("0","").replace("1","").replace("2","").replace("3","").replace("4","").replace("5","").replace("6","").replace("7","").replace("8","").replace("9","")
        
        # (Gesamt-)Anzahl Worte zählen mit len()
        def worte_anzahl(text):
            text=file_content.split()
            zaehlen=len(text)
            
            return zaehlen
        
       #wie oft jedes Wort vorkommt -> mit .split()
        def wortvorkommen(text):
            anzahl={}
            text=file_content.split()
            
            for wort in text:
                if wort in anzahl:
                    anzahl[wort] = anzahl[wort] + 1
                
                else:
                    anzahl[wort] = 1
            
            return anzahl
 
        # Anzahl Vorkommen der Buchstaben -> .split() darf hier nicht rein wirken, sonst klappt es nicht
        #alphabetisch geordnet und untereinander gelistet wäre noch schön, klappt noch nicht
        def buchstabenvorkommen(text):
            anzahl={}
            text=file_content
            
            for buchstabe in text:
                if buchstabe in anzahl:
                    anzahl[buchstabe] = anzahl[buchstabe] + 1
                
                else:
                    anzahl[buchstabe] = 1
            
            return anzahl
 
            
print("Gesamtanzahl Worte: ", worte_anzahl(eingabe))
print("Vorkommen der Worte: ", wortvorkommen(eingabe))
print("Anzahl Buchstaben: ", buchstabenvorkommen(eingabe))


# Ergebnisse in CSV-Files schreiben: wort.csv -> funktioniert noch nicht :-(
#es macht eine Datei, schreibt aber nichts rein
#liste machen, damit "values" (for row in values:) geschrieben werden kann
l_wortvorkommen = []
for wort in anzahl.values():
    l_wortvorkommen.append([wort])
    
with open ("wort.csv", "w") as f_wort:
    csv_writer = writer(f_wort)
    for row in l_wortvorkommen:
        csv_writer.writerow(row)


# Ergebnisse in CSV_Files schreiben: buchstabe.csv -> funktioniert noch nicht:
# es erstellt Datei, schreibt aber nichts rein
l_buchstabenvorkommen = []
for buchstabe in anzahl.values():
    l_buchstabenvorkommen.append([buchstabe])

with open ("buchstabe.csv", "w") as f_buchstabe:
    csv_writer = writer(f_buchstabe)
    for row in l_buchstabenvorkommen:
        csv_writer.writerow(row)