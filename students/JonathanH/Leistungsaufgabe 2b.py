# Leistungsaufgabe 2b (das 1 mal 1)
a=1
b=1 # b ist also die Reihe (z.B. 2er Reihe)

def a_mal_b():
    
    # Äussere Schleife für b-er Reihe
    for b in range(1, 11):
        print(str(b) + " er Reihe:")
        
        #innere Schleife für multiplizieren mit a
        for a in range(1, 11):
            multipli = a * b
            print(str(a), "x " + str(b) + " = " + str(multipli))
            
            a=a+1
        
        b=b+1

        print("--------")

print(a_mal_b())  # In Shell zeigt es nach letztem Strich None an. Warum?
