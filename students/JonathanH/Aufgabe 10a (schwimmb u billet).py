# Übungsbeispiele zu Funktionen u. Kontrollstrukturen
# Beispiel 10a (für billet preis u schwimmbad) -> Habe nicht genau verstanden, was sich wiederholen soll
# und warum? Und was ist mit "gewählte Option" gemeint?

#Billet SBB
# Eingaben:
alt=int(input("Bitte Alter angeben: "))
entfernung=float(input("Entfernung in km angeben: "))

# Funktion zur Berechnung des Billetpreises -> alter (a), entfernung (e)
def billet_preis(a, e):
    grundpreis=2 + (0.25*e)
    
    if a<16:
         preis=grundpreis/2
    
    elif a<6:
         preis=0
    
    else:
        preis=grundpreis
    
    return preis


#Schwimmbad
#Eingabemöglichkeiten:
kar=int(input("Welche Karte? (Tageskarte = 2 / Nachmittagskarte = 3 / Kurzzeitkarte = 4)"))
rab=int(input("Ermaessigungskarte? (ja = 1 / nein = 0)"))

# Funktion zur Berechnung des Billetpreises Schwimmbad
def preis_karte(karte, alter, rabatt):
    
    # Die verschiedenen karten in ihren Grundpreisen (ohne rabatt, für erwachsene)
    if karte==2:
        preis=10
    
    elif karte==3:
         preis=6
        
    elif karte==4:
         preis=5
         
    else:
        preis=0
    
    # Die Position "alter":
    if alter <=6:
        preis=0
    
    elif alter <=12:
        preis=preis/2
    
    else:
        preis=preis
    
    # rabatte
    if rabatt==1:
         preis=preis + (preis*0.1)
         
    elif rabatt==0:
        preis=preis
    
    return preis


print("Die Fahrkarte kostet: ", billet_preis(alt, entfernung))
print("Der Eintritt für das Schwimmbad ist: ", preis_karte(kar, alt, rab))
