# ad hoc Übung 2.9 c) (Dreiecke mit verschachtelten Schleifen, mit Benutzereingabe Anzahl Dreiecke,
# sowie Anzahl Reihen (Folie 35)
# ACHTUNG nochmals anschauen;: irgendwas stimmt noch nicht;
# anstelle von Reihen macht es einen abstrakten Tannenbaum :-/


from turtle import*
reset()

#Benutzereingabe Anzahl Dreiecke und wieviele Dreiecke pro Reihe
anzahl=numinput("Eingabe 1:", "Wieviele Dreiecke pro Reihe? ")
reihen=numinput("Eingabe 2:", "Wieviele Reihen? ")

# Funktion Dreieck für zeichnen in Reihe
def dreieck_reihe(ffarbe, stfarbe, stdicke, s, winkel):
    fillcolor(ffarbe)
    begin_fill()
    for count in range(3):
        fd(s)
        left(winkel)
    end_fill()
    penup()
    fd(s + s/3)
    pendown()
    

# Funktion Dreieck für Anzahl Reihe (nach unten also)
def dreieck_weitere(ffarbe, stfarbe, stdicke, s, winkel):
    fillcolor(ffarbe)
    begin_fill()
    for count in range(3):
        fd(s)
        left(winkel)
    end_fill()
    penup()
    fd(s + s/3)
    pendown()
    
#zurück zu Anfang Reihe und neuer Startpunkt
def zurueck(s):
    penup()
    rt(180)
    fd(s + s/3)
    lt(90)
    fd(20)
    lt(90)
    pendown()       
  

# Das Dreieck xmal hintereinander zeichnen (je nach benutzer-input) (d.h. i Anzahl Dreiecke pro Reihe)
for count in range(int(anzahl)):
    dreieck_reihe("steelblue", "blue", 2, 100, 120)
    zurueck(100)
    

# ii wie viele Reihen
for count in range (int(reihen)):
    dreieck_weitere("green", "blue", 2, 100, 120)
    zurueck(100)

hideturtle()

exitonclick()