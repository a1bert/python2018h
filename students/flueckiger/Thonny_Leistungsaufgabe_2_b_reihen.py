
def reihen():
    i = 1 # i ist multiplikator
    y = 1 # y ist die reihe
    for count in range(10): # äussere for-schleife: gibt überschrift und trennstrich aus
                            # erhöht am schluss die reihe (y) auf y + 1 und setzt i auf 1 zurück
        print(str(y) + "er Reihe:")
        for count in range(10): # innere for-schleife: gibt die zahlen i x y sowie das resultat aus
                                # erhöht am schluss i auf i + 1
            print(str(i) + " x " + str(y) + " = " + str(i * y))
            i = i + 1 # multiplikator i wird auf i + 1 erhöht
        y = y + 1 # reihe (y) wird auf y + 1 erhöht
        i = 1 # i wird auf 1 zurückgesetzt
        print("____________")
        
reihen()
    
            
        
        
        
        
reihen()