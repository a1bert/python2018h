# zuerst funktionen schreiben: menüpunkte 1 - 3, wahl menüpunkt und hauptmenü
# hauptmenü am ende jeder funktion wieder aufrufen
# schliesslich nur das hauptmenü im hauptprogramm aufrufen


einkaufsliste = []

# menüpunkt 1
def items_eingeben():
    item = 0
    while item != "x":
        item = input("produkt eingeben: ")
        if item != "x":
            einkaufsliste.append(item)
        elif item == "x":
            break
    hauptmenue()
               

# menüpunkt 2
def items_korrigieren():
    print("Artikel korrigieren (k) oder löschen (l)?")
    eingabe = input("Eingabe: ")
    while eingabe != "k" or "l":
        if eingabe == "k":
            for nr, item in enumerate(einkaufsliste):
                print(nr, item)
            eingabe_nr = int(input("Bitte Artikelnummer eingeben: "))
            eingabe_bez = input("Bitte korrigieren: ")
            einkaufsliste[eingabe_nr] = eingabe_bez
            items_ausgeben()
        elif eingabe == "l":
            for nr, item in enumerate(einkaufsliste):
                print(nr, item)
            eingabe = int(input("Bitte Artikelnummer eingeben: "))
            del einkaufsliste[eingabe]
            items_ausgeben()
        else:
            items_korrigieren()
    hauptmenue()
    
 
# menüpunkt 3
def items_ausgeben(): 
    einkaufsliste.sort()
    print("*****")
    print("Einkaufsliste: ")
    for nr, item in enumerate(einkaufsliste):
        print(nr, item)
    print("*****")   
    hauptmenue()

def hauptmenue():
    print("""
    Menü
    _______________
    
    1. Artikel eingeben
    2. Artikel prüfen
    3. Artikel ausgeben
    4. Exit
    """)
    wahl()

def wahl():
    wahl = int(input("Wählen Sie den Menüpunkt: ")) # int nicht vergessen!
    if wahl == 1:
        items_eingeben()
    elif wahl == 2:
        items_korrigieren()
    elif wahl == 3:
        items_ausgeben()
    elif wahl == 4: # menüpunkt 4
        print("""
        *****
        
        Programm Ende
        
        *****
        """)
    else:
        print("Bitte eine Zahl 1 bis 4 eingeben: ")
        hauptmenue()

# Hauptprogramm
hauptmenue()



        