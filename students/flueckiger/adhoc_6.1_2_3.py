# aufgabe 2 : programm so verändern, dass in der zieldatei bei jeder zeile zuerst die Zeilennummer gefolgt von einem Doppelpunkt steht

# aufgabe 3: fehlermeldungen ausgeben, wenn datei nicht geöffnet oder geschrieben werden konnte

try:

    with open("adhoc 6.1 datei.txt", "r") as source: # öffnet das dokument zum lesen
        with open("adhoc 6.1 datei kopie.txt", "w") as copy: # kreiert neues dokument
            for no, line in enumerate(source): # es wird zeile für zeile gelesen
                copy.write("%d: %s" % (no + 1, line)) # zeilen werden ins zieldokument geschrieben, zählung beginnt bei 1

       
except FileNotFoundError:
    print("Datei konnte nicht geöffnet werden")
except PermissionError:
    print("Datei konnte nicht geschrieben werden")
                
          
            
        
    
    
