from random import randint


# mit while

i = 1
while i <= 6:
    zufallszahl = randint(1, 42)
    print(str(i) + ". " + str(zufallszahl))
    i = i + 1
    
# mit for
    
i = 1
for i in range(6):
    zufallszahl = randint(1, 42)
    print(str(i + 1) + ". " + str(zufallszahl))

    