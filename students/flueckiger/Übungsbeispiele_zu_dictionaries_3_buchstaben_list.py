# problem 1: wie gebe ich die liste mit bindestrich aus?
# problem 2: wie gebe ich wort in fehlermeldung zusammenhängend aus? "übung"
# problem 3: wieso wird "Ausgabe" auch bei fehlermeldung gedruckt?

# main dictionary
dict = {"a": "alfa", "b": "bravo", "c": "charlie", "d": "delta", "e": "echo", "f": "foxtrot", "g": "golf", "h": "hotel", "i": "india", "j": "juliett", "k": "kilo", "l": "lima", "m": "mike", "n": "november", "o": "oscar", "p": "papa", "q": "quebec", "r": "romeo", "s": "sierra", "t": "tango", "u": "uniform", "v": "victor", "w": "whiskey", "x": "x-ray", "y": "yankee", "z": "zulu"}

# a
eingabe = input("Welches Wort soll ich buchstabieren?: ").lower()
eingabe = list(eingabe)
wortliste = []
for buchstabe in eingabe:
    if buchstabe not in dict.keys():
        print("Kann", eingabe, "nicht buchstabieren, da", buchstabe, "nicht definiert wurde.")
        break
    elif buchstabe in dict.keys():
        wortliste.append(dict[buchstabe]) # das passende wort aus dem dict wird in die wortliste eingefügt
print("Ausgabe: ", wortliste)

# b
eingabe = input("Welches Wort soll ich buchstabieren?: ").lower()
umlaute = {"ä": "a", "ö": "o", "ü": "u"} # dictionary für umlaute
eingabe = list(eingabe)

wortliste = []
for buchstabe in eingabe:
    if buchstabe not in dict.keys(): # wenn der buchstabe ein umlaut ist dann....
       buchstabe = umlaute[buchstabe] # umlaut wird zum wert aus dictionary "umlaute"
       wortliste.append(dict[buchstabe])
    elif buchstabe in dict.keys():
        wortliste.append(dict[buchstabe]) # das passende wort aus dem dict wird in die wortliste eingefügt
print("Ausgabe: ", wortliste)    

    

