# schreiben sie ein programm, welches über die folgende zweidimensionale liste iteriert und alle elemente wie folgt ausgibt:
# l = [["Brot", 2.1], ["1 kg Organgen", 3.5], ["Schoki", 5.5]]


l = [["Brot", 2.1], ["1 kg Organgen", 3.5], ["Schoki", 5.5]]

def produkte_liste():
    for i in l:
        print(i[0], "kostet", i[1], "CHF.")
        
def ausgabe():
    print("Ausgabe:")
    print("------")
    produkte_liste() # aufrufen der funktion produkte_liste
    
# hauptprogramm

ausgabe()
        
