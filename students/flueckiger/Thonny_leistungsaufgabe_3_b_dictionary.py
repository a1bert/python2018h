
# dictionary
dict = {"zahnarzt": "dentist", "haus": "house", "zahn": "dent", "schmerz": "pain", "tod": "death", "mund": "mouth", "zahnfleisch": "gums", "karies": "caries", "zahnbürste": "toothbrush", "zahnpasta": "toothpaste"}

# funktion
def abfrage():
    richtig = 0
    for de, en in dict.items(): # for schleife: fragt jeden deutschen begriff im dictionary ab
        eingabe = input("Was heisst " + de +"?: ")
        if eingabe == en:
            richtig = richtig + 1
            print("RICHTIG!")
        elif eingabe != en:
            print("FALSCH!")
    print("Sie haben", richtig, "von 10 Fragen richtig beantwortet!")

# Hauptprogramm
abfrage()


