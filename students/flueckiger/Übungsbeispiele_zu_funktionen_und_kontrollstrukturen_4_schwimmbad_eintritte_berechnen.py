
def berechnet_eintrittspreis(kartentyp, ermaessigungskarte, alter):
    if kartentyp == "kurzzeitkarte":
       preis = 5
    elif kartentyp == "nachmittagskarte":
        preis = 6
    elif kartentyp == "tageskarte":
        preis = 10
    
    if alter <= 6:
        return 0.0
    elif alter <= 12:
        preis = preis/2
    
    if ermaessigungskarte:
        return preis * 0.9
    elif ermaessigungskarte != True:
        return preis

                   
print(berechnet_eintrittspreis("kurzzeitkarte", False, 30))
print(berechnet_eintrittspreis("tageskarte", False, 2))
print(berechnet_eintrittspreis("tageskarte", True, 12))