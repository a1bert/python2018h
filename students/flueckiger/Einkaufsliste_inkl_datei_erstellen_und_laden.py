# Notizen:
# zuerst funktionen schreiben: menüpunkte 1 - 3, wahl menüpunkt und hauptmenü
# def hauptmenü muss an den schluss, da dort funktionen aufgerufen werden, die bereits definiert sein müssen
# hauptmenü am ende jeder funktion wieder aufrufen
# schliesslich nur das hauptmenü im hauptprogramm aufrufen

# ____________________________________________________________________________________________
# Hausübungsbeispiel zur achten einheit: items werden automatisch aus einkaufsliste.txt geladen
# _____________________________________________________________________________________________
def liste_laden():
    try: 
        with open("einkaufsliste.txt") as liste:
            for item in liste:
                einkaufsliste.append(item) 
            print("Ihre Einkaufsliste: ", einkaufsliste)
    except FileNotFoundError:
        print("Die Einkaufsliste ist leer")
    hauptmenue()


# menüpunkt 1
def items_eingeben():
    item = 0
    while item != "x":
        item = input("produkt eingeben: ")
        if item != "x":
            einkaufsliste.append(item)
        elif item == "x":
            break
    hauptmenue()
               

# menüpunkt 2

def items_korrigieren():
# oder:
# eingabe = ""
# while eingabe != "k" and eingabe != "l":
# eingabe = input("...")
    print("Artikel korrigieren (k) oder löschen (l)?")
    eingabe = input("Eingabe: ")
    while eingabe != "k" or "l":
        if eingabe == "k":
            for nr, item in enumerate(einkaufsliste):
                print(nr, item)
            eingabe_nr = int(input("Bitte Artikelnummer eingeben: "))
            eingabe_bez = input("Bitte korrigieren: ")
            einkaufsliste[eingabe_nr] = eingabe_bez
            items_ausgeben()
        elif eingabe == "l":
            for nr, item in enumerate(einkaufsliste):
                print(nr, item)
            eingabe = int(input("Bitte Artikelnummer eingeben: "))
            del einkaufsliste[eingabe]
            items_ausgeben()
        else:
            items_korrigieren()
    
    
 
# menüpunkt 3

def items_ausgeben(): 
    einkaufsliste.sort()
    print("*****")
    print("Einkaufsliste: ")
    for nr, item in enumerate(einkaufsliste):
        print(nr, item)
    print("*****")   
    hauptmenue()

# menüpunkt 4
def programmende():
    print("""
        *****
        
        Programm Ende
        
        *****
        """)
    # _________________________________________________________________________________
    # Hausübungsbeispiel zur achten einheit: artikel in datei einkaufsliste.txt speichern
    # __________________________________________________________________________________
    with open ("einkaufsliste.txt", "w") as liste:
        for line in einkaufsliste:
            liste.write(line)
            liste.write("\n") # Achtung: es entstehen viele Zeilenabstände!
            
            
    

def hauptmenue():

    print("""
    Menü
    _______________
    
    1. Artikel eingeben
    2. Artikel prüfen
    3. Artikel ausgeben
    4. Exit
    """)
    
    wahl()

def wahl():
    wahl = int(input("Wählen Sie den Menüpunkt: ")) # int nicht vergessen!
    if wahl == 1:
        items_eingeben()
    elif wahl == 2:
        items_korrigieren()
    elif wahl == 3:
        items_ausgeben()
    elif wahl == 4: # menüpunkt 4
        programmende()

    else:
        print("Bitte eine Zahl 1 bis 4 eingeben: ")
        hauptmenue()

# Hauptprogramm
einkaufsliste = []
liste_laden()





        