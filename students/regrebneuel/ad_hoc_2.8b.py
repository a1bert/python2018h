# uebung 2.8b ad hoc

#from math import *

def rechne(eingabe): #
    minimum = min(eingabe)
    maximum = max(eingabe)
    durchschnitt = sum(eingabe)/len(eingabe)
    return minimum, maximum, round(durchschnitt, 2)
    

zahlenreihe = [12, 23, 5, 7, 8, 56, 77, 18,9] #koennte allenfalls auch als benutzerabfrage uebergeben werden
ausgabe = rechne(zahlenreihe)
print("Minimum: %d \nMaximum: %d \nDurchschnitt: %.2f" % (ausgabe[0], ausgabe[1], ausgabe[2]))