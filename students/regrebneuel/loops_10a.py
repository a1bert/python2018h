#loops.pdf aufgabe 10 mit uebung 4 und uebung3

#Menueaufbau
def menu():
        
    print("=============================================")
    print("Programmueersicht:")
    print("  1 ... Preis fuer Fahrkarte berechnen")
    print("  2 ... Eintritt fuer das Schwimmbad berechnen")
    print("\n  0 ... Beenden")
    print("=============================================")
    choice = input("Gewaehlte Option: ")
    return choice

#Billetpreis
def get_billet_preis(alter, entf):
    grundpreis = 2
    fahrpreis = grundpreis + int(entf)*.25
    if alter <= 6:
        billet_preis = float(fahrpreis * 0)
    elif alter < 16:
        billet_preis = round(float(fahrpreis * 0.5)*2)/2
    else:
        billet_preis = int(fahrpreis)

    return print("Das Ticket kostet %.2f CHF." %(billet_preis))#ausgabe Fahrkartenpreis aus Funktion    

#Schwimmbadeintritt
karten = [[5, "kurzzeitkarte"], [10,"tageskarte"], [6,"nachmittagskarte"]]

def eintrittspreis(eintritt, ermaessigungskarte, alter):
    ermaessigung = 0
    rabatt = 0
    for karte in karten:
        if karte[1] == eintritt:
            break
    if ermaessigungskarte == True:
        ermaessigung = 10
    if alter <= 6:
        rabatt = 100
    elif alter <12:
        rabatt = 50
    else:
        rabatt = 0
    ermaessigung = rabatt + ermaessigung
    preis = round((karte[0] - ermaessigung * karte[0]/100) * 2) / 2
        
    return print ("%s; Total Ermaessigung: %d%%; Eintrittspreis: %.2f CHF" % (eintritt.capitalize(), ermaessigung, preis))#ausgabe Eintrittspreis aus Funktion

def check_ticket2():#prueffunktion zur ticketeingabe schwimmbadeintritt: gibt es das ticket in der liste?
    ticket = input("Tageskarte, Nachmittagskarte oder Kurzzeitkarte?").lower()
    ticketinput = 0 #pruefvariable, solange die unveraendert ist, wird die Abfrage wiederholt, wird sie str(0)-> programmende, sonst ticketname fuer programmablauf
    if ticket == '0':#abbrechen, wenn 0 eingegeben wird
        print("Sie haben das Programm beendet.")
        return '0'
    for i in 'tageskarte', 'nachmittagskarte', 'kurzzeitkarte':
        if ticket == i:
            ticketinput = ticket
            return ticketinput
    if ticketinput == 0: # erneute abfrage bei falscheingabe
        print("Es gibt nur diese drei zur Auswahl! Bitte waehlen: ")
        return check_ticket2()

def main():# uebernimmt die eingabe aus dem menu und verteilt je nach eingabe an eintrittspreis() oder get_billetpreis()
    eingabe = menu()
    if eingabe == '2': # nutzer fragt nach schwimmbadeintrittspreis
        print("Eintritt fuer Schwimmbad berechnen")
        print("----------------------------------")
        ticket = check_ticket2() #pruefschlaufe fuer richtigen ticketnamen in eigener funktion
        if ticket == '0': #abbruch falls 0 eingegeben
            return
        elif input("Haben Sie eine Ermaessigungskarte? J/N: ").lower() == 'j':
            discount = True
        else:
            discount = False
        age = int(input("Alter? "))
        eintrittspreis(ticket, discount, age) #Resultatausgabe eintrittspreis
        return main()
    elif eingabe == '1': #nutzer fragt nach fahrkarte
        print("Preis fuer Fahrkarte berechnen")
        print("------------------------------")
        age =  int(input("Wie alt sind Sie? "))
        distance = int(input("Wie weit fahren Sie in km? "))
        get_billet_preis(age, distance)
        return main()
            
    elif eingabe == '0':
        print("Sie haben das Programm beendet.")
        return
        
    else:
        print("Bitte nur die angegebenen Zahlen auswaehlen!")
        return main()#erneute abfrage bei falscheingabe

main() #programstart