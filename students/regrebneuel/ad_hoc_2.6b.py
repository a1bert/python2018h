#Monatsabfrage
monate = [[1, "Januar"],[2, "Februar"],[3, "MÃ¤rz"], [4, "April"], [5,"Mai"], [6, "Juni"], [7, "Juli"], [8, "August"], [9, "September"], [10, "Oktober"], [11, "November"], [12,"Dezember"]]
# Monate mit Zugriffszahl in mehrdimensionaler Liste. Abfrage Ã¼ber Vergleich mit der Zugriffszahl, Ausgabe bei Ãbereinstimmung

def abfrage():
    d = input("Bitte Zahl zwischen 1 und 12 eingeben, 99 zum Beenden: ")
    if d.isdigit() == False: #PrÃ¼fung der Eingabe
        print(";-) Eine Zahl war gesucht. ")
        return abfrage()
    else:
        d = int(d)
    if 0 < d <= 12:
        ausgabe = str(d) + " ist der "+ monate[d-1][1] + "."
        return print(ausgabe), abfrage()
    elif d == 99: # Eingabe von 99 beendet das Programm
        ende = "Monatsabfrage beendet, Danke."
        return ende
    else:
         # rekursive Schleife, solange keine Zahl zw. 1 und 12 eingegeben wird
        return abfrage()

print(abfrage())
 
