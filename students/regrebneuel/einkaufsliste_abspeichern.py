'''
c) erweitern sie das beispiel zur einkaufsliste (hausÃ¼bung zur 6.
einheit), sodass
     i) die items auf der einkaufsliste beim verlassen des programmes
in der datei "einkaufsliste.txt" gespeichert und
    ii) beim erneuten ausfÃ¼hren des programmes automatisch aus
"einkausliste.txt" geladen werden, wenn die datei existiert.

    hinweis: die eingelesenen zeilen enthalten am ende immer einen
zeilenumbruch. mittels `strip()` befehl kann dieser entfernt werden.
'''
#Einkaufsliste: item oder x fÃ¼r einkaufsliste (Apfel, Birne, Kirsche, Banane) abfragen, gibt Einkaufsliste aus



#menÃ¼punkt1
def items_eingeben(ekliste):
    print("--------------------------")
    while True:
        eingabe = input("Item: ")
        if eingabe == 'x':
            break
        ekliste.append(eingabe)
    #print("\n")        
    return ekliste

#menÃ¼punk3
def items_ausgeben(ekliste):
    ekliste.sort()
    print("""
==============
Einkaufsliste:
--------------""")
    if ekliste == []:
        print("""
         Die Liste ist noch leer!
        Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°""")
        return 
        
    
    for item in ekliste:
        print(item)
    print("==============\n")
    return ekliste

#items prÃ¼fen Liste ausgeben mit MenÃ¼ fÃ¼r Korrektur
#mit 0 fÃ¼r Exit ins HauptmenÃ¼
# 2 (l)Ã¶schen oder (k)orrigieren
def items_korrigieren(ekliste):
    trigger = input("(" + "lÃ¶schen"[0] + ")" + "lÃ¶schen"[1:] + ", " + "(" + "korrigieren"[0] + ")" + "korrigieren"[1:] + " oder " + "beenden mit 0" + "? ")
    
    #case 1 - lÃ¶schen
    if trigger == 'l':
        sel = ""
        itemno = list(enumerate(ekliste))
        print("==============\n")
        for no in itemno:
            print(no[0]+1, no[1])
        print("==============")
        sel = int(input("Welches Element mÃ¶chten Sie lÃ¶schen? "))
        if sel != 0:
            del ekliste[sel-1]
        return ekliste
    
    #case 2 - korrigieren
    if trigger == 'k':
        sel = ""
        itemno = list(enumerate(ekliste))
        print("==============")
        for no in itemno:
            print(no[0]+1, no[1])
        print("==============")
        sel = int(input("Welches Element mÃ¶chten Sie korrigieren? "))
        if sel != 0:
            ekliste[sel-1] = input("Korrektur: ")
        return ekliste
    
    if trigger in ['b', 0, 'x']:
        return 

def main():
    liste = []
    while True:
        
        input = menu(liste)
        
        if input[0] == 1:
                  liste = items_eingeben(input[1])
        elif input[0] == 2:
                  liste = items_korrigieren(input[1])
        elif input[0] == 3:
                  liste = items_ausgeben(input[1])
        elif input[0] == 4:
            try:
                with open("einkaufsliste.txt", "w") as f:
                    for line in sorted(liste):
                        line = f.write(line + "\n")
                        #line = f.strip()
            except FileNotFoundError:
                print("Datei nicht gefunden.")
            except PermissionError:
                print("Keine Schreibberechtigung.")
            print("""
     # # # # # # # # # # # # #
     #    Programm beendet   #
     # # # # # # # # # # # # #""")
            return
        
        else:
                  print("ungÃ¼ltige Wahl!")
                  print("::::nochmal::::")
              
def menu(liste):
    if liste == []:
        try:
            with open("einkaufsliste.txt", "r") as source:
                for line in source:
                    line = line.strip()
                    liste.append(line)
                #print(liste)
                items_ausgeben(liste)
        except FileNotFoundError:
            print("Die Einkaufsliste wurde noch nicht gespeichert.")
    
        
    print("""
-------------------------
Auswahl:
    1 ... Items eingeben
    2 ... Items prÃ¼fen
    3 ... Items ausgeben
    4 ... Speichern/Exit
--------------------------""")
    choice = int(input("GewÃ¤hlte Option: "))
    return choice, liste

main()          

