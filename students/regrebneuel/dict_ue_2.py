# dict uebung 2
'''
Wortstatistik: Schreiben Sie eine Funktion, welche einen Text entgegennimmt
und alle WÃ¶rter mit deren HÃ¤ufigkeit ausgibt. Die Gross-
/Kleinschreibung soll dabei nicht berÃ¼cksichtigt werden.
'''

l_input = "Der Tag begann sehr gut! Der Morgen war schÃ¶n."

def word_stat(input):
    list = input.split(" ")
    #print(list)
    d = {}
    
    for word in list:
        if word[-1] == '.' or word[-1] == ',' or word[-1] == '!':#satzzeichen raus
            word = word[:-1]
        if word in d: # wenn das wort schon im dictionary ist, weiter in der schleife
            continue
        d[word] = list.count(word.lower())
    print(d)
    return

word_stat(l_input)
            