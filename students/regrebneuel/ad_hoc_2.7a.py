#zeichnen mit turtle Grafikbibliothek
from turtle import *
#seitenlÃ¤nge, stiftgrÃ¶sse
sl =100
pensize(5)
speed(100)
#farben
pencolors = ["red", "green", "blue"]
fillcolors = ["cyan", "yellow", "beige"]

# a) dreieck als funktion
def dreieck(sl, pc, fc):
    begin_fill()
    pencolor(pc)
    fillcolor(fc)
    lt(60)
    for i in range(3):
        fd(sl)
        lt(120)
    lt(60)    
    end_fill()
    

for d in range(3):
    dreieck(sl, pencolors[d], fillcolors[d])



hideturtle()
#reset()
onscreenclick(reset())

#b) Quadrate als Funktion
pensize(5); pencolor("red"); speed(100)
seitenlaenge = numinput("Eingabeaufforderung","Bitte SeitenlÃ¤nge des Anfangsquadrats eingeben:",
           "Pixel")
def quadrat(seitenlaenge, fc):
    fillcolor(fc)
    begin_fill()
    for i in range (4):
        fd(seitenlaenge)
        lt(90)
    end_fill()
    rt(360/5)

rt(45)
quadrat(seitenlaenge, "cyan")
quadrat(seitenlaenge, "yellow")
quadrat(seitenlaenge, "magenta")
quadrat(seitenlaenge, "blue")
quadrat(seitenlaenge, "lawn green")

hideturtle()

  
    
exitonclick()