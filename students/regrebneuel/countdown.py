#countdown mit while

i=10
while i>=0:
    print(i)
    i -= 1 # i = i - 1
    
print("Fertig!")



#countdown mit while und if
import time
i=10
while i>=0:
    if i == 5:
        print(str(i) + " Nur noch 5 Sekunden!")
        i -= 1
    elif i == 1:
        print(str(i) + " Jetzt ist es bald soweit!")
        i -= 1
    else:
        print(i)
        i -= 1
    time.sleep(.2)        
print("Fertig!")              
              
              
              