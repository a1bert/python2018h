'''
Leistungsaufgabe { Text Statistiken
1. Schreiben sie ein Programm, dass eine URL entgegenimmt (zum Beispiel
http://www.ietf.org/rfc/rfc2616.txt) und
(a) jedes Wort und die Anzahl der Vorkommnisse des Wortes berechnet.
(b) berechnet wie oft jeder Buchstaben vorkommt.
2. Schreiben sie die Ergebnisse in zwei CSV Files:
3. Auswahl Ressource
'''

from urllib.request import urlopen #url-funktionalitÃ¤t laden
from csv import writer # umd CSV zu schreiben

# daten vom web holen, von satzzeichen bereinigen, wÃ¶rter als liste
def get_web_data(source):
    with urlopen(source) as src:
        data = src.read().decode("utf-8")
        print(data)
    data_clean = data.replace('\n',' ').replace('.','').replace(',','').replace(':','').replace(';','').replace('?','').replace('!','').replace('\n\n','\n').replace('\t',' ')
#viele leere strings bleiben stehen...?
    return data_clean

def get_file_data(source):
    with open(source, 'r') as src:
        data = src.replace('\n',' ').replace('.','').replace(',','').replace(':','').replace(';','').replace('?','').replace('!','').replace('\n\n','\n').replace('\t',' ')
#viele leere strings bleiben stehen...?
    return data

#wortstatistik, welches wort wie of
def word_stat(input):
    words = input.split(' ')#string in liste bei leerschlÃ¤gen aufteilen
    count = {} #dictionary fÃ¼r wÃ¶rter und hÃ¤ufigkeit
    for word in sorted(words):
        if word != '' and word in count: # wenn das wort schon im dictionary ist, weiter in der schleife
            count[word] = count[word] + 1
        elif word != '':
            count[word] = 1
    values = []
    for key, value in count.items(): # dictionary in liste schreiben
        values.append([key, value])
    
    try: #csv schreiben mit liste
        with open('word_count.csv', 'w') as f:
            csv_writer = writer(f)
            for row in values:
                csv_writer.writerow(row)
    except FileNotFoundError:
        print("word_count.csv is missing.")
    except PermissionError:
        print("You are not allowed to write to word_count.csv.") 
#    print(count)
    return count

def char_count(inputstring):
    alpha = list("abcdefghijklmnopqrstuvwxyz")# alphabet als liste Ã¼bergeben
    chars = {} #dictionary fÃ¼r die Buchstaben und HÃ¤ufigkeit
    for bst in alpha: #durchs alphabet iterieren und pro bst zÃ¤hlen
        if bst in inputstring:
            chars[bst] = inputstring.count(bst) #zÃ¤hlt, wie oft der bst im eingabestring vorkommt
    values = [] #dasselbe in grÃ¼n fÃ¼r Buchstaben wie oben fÃ¼r wÃ¶rter
    for key, value in chars.items():
        values.append([key, value])
    try:
        with open('char_count.csv', 'w') as f:
            csv_writer = writer(f)
            for row in values:
                csv_writer.writerow(row)
    except FileNotFoundError:
        print("char_count.csv is missing.")
    except PermissionError:
        print("You are not allowed to write to word_count.csv.") 
        characters.append()
    print(chars)
     
userinput = input("Bitte Datenquelle angeben: ")

if userinput[0:8] == 'https://':
    input = get_web_data(userinput)
else:
    input = get_filedata(userinput)

wordcount = word_stat(input)
char_count(input)