# dict_uebung 3a
'''
Die \International Civil Aviation Organization (ICAO) Alphabet weist
Buchstaben Wortern zu1, damit diese fehlerfrei buchstabiert werden
kÃ¶nnen. Schreiben Sie ein Programm, das ein beliebiges Wort buchstabiert.
Welches Wort soll ich buchstabieren: Synthese
Ausgabe: Sierra-Yankee-November-Tango-Hotel-Echo-Sierra-Echo
'''
def alpha_icao_einlesen(): #vorbereitung dictionary
    icao = {}
    prep = []
    try:
        with open("icao.txt", 'r') as source:
            for line in source: #zeilenweise einlesen
                line = line.strip() # newline am Ende der Zeile weg
                line = line.replace(' - ',': ') # vorbereiten fÃ¼r dictionary-Eintrag
                prep.append(line)
    except FileNotFoundError:
            print("Datei nicht gefunden.")
    for i in prep: # Schleife Ã¼ber Prep-Liste
        key = i[0].lower() # Variable des SchlÃ¼ssels als Kleinbuchstaben
        icao[key] = i[3:] # String zum Buchstabieren ab drittem Zeichen aus Prep-Liste
    #print(icao) # schreibt das Buchstabier-Dictionary
    return icao

def spelling_bee(alpha):
    #umlaute = {'Ã¤':'ae','Ã¶': 'oe', 'Ã¼':'ue'} # dictionary fÃ¼r Umlautmapping
    eingabe = input("Welches Wort soll ich buchstabieren? ")
    #quick and dirty
    eingabe = eingabe.replace('Ã¶', 'oe').replace('Ã¤', 'ae').replace('Ã¼', 'ue') 
    spell = eingabe.lower() # Abfragestring als Minuskeln
    ausgabe = "" # Definition Variable fÃ¼r Ausgabestring
    
    ## unnÃ¶tig mit der quick and dirty lÃ¶sung
##    umlaut = '' # Definition Variable fÃ¼r alternative Abfrage bei Umlauten
##    for letter in spell: # PrÃ¼fen ob Umlaute in der Eingabe
##        if letter in umlaute:
##            for key, value in umlaute.items():
##                letter = value
##                continue
##        umlaut = umlaut+letter # Aufbau des umlautfÃ¤higen Abfragestrings
            
    for letter in spell: # schleife durch den Abfragestring
            for index, word in alpha.items(): # Schleife durch das Buchstabierdictionary
                if letter == index: 
                    ausgabe = ausgabe+word+"-" # Verkettung des Ausgabestrings
                    continue

    print(ausgabe[:-1]) #[:-1] entfernt den letzten strich von der schleife
    return

spelling_bee(alpha_icao_einlesen())


