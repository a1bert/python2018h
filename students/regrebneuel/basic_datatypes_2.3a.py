# basic datatypes uebung 2.3.a
'''
Problem: das dictionary im loop Ã¤ndern wirft einen RunTimeError:
dictionary changed size during iteration,
auch wenn man es kopiert. zwei lÃ¶sungen:
'''
#var1
lm = {"Milch": "2.1", "Brot": "3.2", "Kaffee": "4.2"}
checklist = ["Brot", "Butter", "Milch"]
def lm_filter(eingabe, ch_list):
    del_keys = []
    output = eingabe
    for word in eingabe:
        if word in ch_list:
            continue
        del_keys.append(word)
    for key in del_keys:
        del output[key]
    print(output)
    return

#var2
def lm_filter2(eingabe, ch_list):
    output = {}
    for word, preis in eingabe.items():
        if word in ch_list:
            output[word]=preis    
        continue
    print(output)
    return

print("var1:")
lm_filter(lm, checklist)
print("var2:")
lm_filter2(lm, checklist)
