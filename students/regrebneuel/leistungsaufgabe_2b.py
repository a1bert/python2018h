#Leistungsaufgabe 2b
import time

def dasgrosseeinmaleins():
        for a in range(1, 11): #schleife fuer die reihe
            print(str(a)+"er-Reihe:")
            time.sleep(0.2)
            for b in range (1, 11): #schleife fuer die multiplikation in der reihe
                reihe = b * a
                print(str(b) + " x " + str(a) + " = " + str(reihe))
                time.sleep(0.2)
               # b+=1 #unnoetig, da for loop in python kein Inkrement braucht (nur while loop)
            print("::::::::::::::")
           # a+=1 #same same
        
dasgrosseeinmaleins()        #aufruf der funktion