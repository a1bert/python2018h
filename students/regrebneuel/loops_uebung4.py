# loops.pdf uebung 4, schwimmbad

karten = [[5, "kurzzeitkarte"], [10,"tageskarte"], [6,"nachmittagskarte"]]

def eintrittspreis(eintritt, ermaessigungskarte, alter):
    ermaessigung = 0
    rabatt = 0
    for karte in karten:
        if karte[1] == eintritt:
            if ermaessigungskarte == True:
                ermaessigung = 10
            if alter <= 6:
                rabatt = 100
            elif alter <12:
                rabatt = 50
            else:
                rabatt = 0
        preis = round((karte[0] - (rabatt + ermaessigung) * karte[0]/100) * 2) / 2
        
    return print ("%s; Rabatt: %d%%; Eintrittspreis: %.2f CHF" % (eintritt.capitalize(), rabatt, preis))
    
eintrittspreis('kurzzeitkarte', False, 30)
eintrittspreis('tageskarte', False, 2)
eintrittspreis('tageskarte', True, 12)
