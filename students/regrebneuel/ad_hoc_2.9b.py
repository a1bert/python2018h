# ad_hoc_2.9
# gle, 2018-11-18
# zeichne dreiecke nach nutzervorgabe: anzahl, groesse, reihen
from turtle import *
speed(10)
# a) dreieck als funktion
def dreieck(sl):
    begin_fill()
    pencolor("blue")
    fillcolor("blue")
    for i in range(3): #schliefe fÃ¼r die seiten
        fd(sl)
        lt(120)
    end_fill()

def start_row(erstereihe): # zeichnet erste reihe
    penup(); goto(startpunkt); pendown()
    for i in range(erstereihe):
        dreieck(groesse)
        penup()
        fd(groesse + groesse * 0.2)
        pendown()

def next_row(dreieckproreihe): # zeichnet die nÃ¤chsten reihen
    nextrowposition = startpunkt - (0, (groesse + groesse * 0.2)) #errechnet startposition fÃ¼r neue reihe
    penup() 
    goto(nextrowposition) #geht in position fÃ¼r nÃ¤chste reihe
    pendown()
    for i in range(dreieckproreihe): #zeichnet reihe
        dreieck(groesse)
        penup();
        fd(groesse + groesse * 0.2);
        pendown()
    return nextrowposition

#Benutzerabfragen, Berechnung der Verteilung
anzahl = int(input("Wie viele Dreiecke hÃ¤tten's denn gern? "))
groesse = int(input("Und wie gross? "))
reihen = anzahl // 4
proreihe = anzahl // reihen # wie viele dreiecke in eine reihe
erstereihe = proreihe + anzahl % reihen # erste reihe faengt ueberzaehlige dreiecke ab

#berechne Startposition gemaess Nutzereingaben
startpunkt = position() + (-(erstereihe * groesse / 2), (reihen * groesse / 2))

# schleife durch reihen zum zeichnen der ersten und weiteren reihen
for d in range(reihen):
    if reihen == 1:
        start_row(erstereihe)
        break
    elif d == 0:
        start_row(erstereihe)
        #print("DEBUG: erste Reihe!", pos(), startpunkt)
    else:
        startpunkt = next_row(proreihe) # weist dem startpunkt die startposition der aktuellen reihe zu und zeichnet naechste reihe
        #print("DEBUG: nÃ¤chste Reihe!", pos(), startpunkt)
        
hideturtle()

exitonclick()


