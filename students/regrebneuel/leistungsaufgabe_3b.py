# leistungsaufgabe 3b
'''
Erstellen Sie mittels einem Dictionary (dict) ein Mini-Deutsch-Englisch WÃ¶rterbuch (10 Vokabeln).
Ein zugehÃ¶rige Vokabeltrainer soll alle Vokabeln nach folgendem Muster abfragen:
Nach Abfrage aller Vokabeln wird noch die Abfragestatistik ausgegeben:
Sie haben 7 von 10 Vokabeln richtig beantwortet!
'''
german = {'Eis': 'ice', 'Haus':'house', 'Maus':'mouse','KÃ¼rbis':'pumpkin','Flasche':'bottle',
          'KÃ¤se':'cheese', 'Schokolade':'chocolate', 'Bank':'bank', 'Tisch':'table','Stuhl':'stool'}

def voci_query(voci_trainer):
    counter = 0
    total = len(voci_trainer)
    for key, word in voci_trainer.items():
        query = input("Was heisst "+key+"? " )
        if query == word:
            counter += 1
            print("RICHTIG!")
            continue
        print("LEIDER FALSCH!")
    print("Sie haben %d von %d richtig beantwortet!" % (counter, len(voci_trainer)))
    
voci_query(german)