# ad_hoc_2.9a
#zeichnen mit turtle Grafikbibliothek
from turtle import *
sl =100
pensize(5)
speed(10)

#  dreieck als funktion mit vorgegebner seitenlaenge
def dreieck(sl):
    begin_fill()
    pencolor("blue")
    fillcolor("blue")
    for i in range(3):
        fd(sl)
        lt(120)
    end_fill()
    
penup(); back(4 * (sl + sl * .2) / 2); pendown() # berechne startposition fuer mittige Verteilung
for d in range(4): #schleife fuer viermaligen funktionsaufruf
    dreieck(sl)
    penup(); fd(sl + sl * .2); pendown() #turtle vorruecken fuer naechstes dreieck

hideturtle()
    
exitonclick()

