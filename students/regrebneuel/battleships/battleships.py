'''
Eine Konsolenvariante des Spiels Battleships, aka Schiffeversenken
mit einer Anspielung auf einen prägenden Achtzigerjahre-Film, der
lernende System schon vorwegnahm.
Viel Vergnügen!
'''
__author__ = "regrebneuel"
__email__ = "info@regrebneuel"
__copyright__ = "Copyright 2018, Zurich"
__license__ = "GPL"
__version__ = "1.0"
__date__ = "032-12-2018"
__status__ = "Beta"
###########################################
import time
### Pretty Printing ###
eins = '''

#####################
##  Spieler EINS:  ##
##  du bist dran!  ##
#####################

'''

zwei = '''

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@      Spieler ZWEI:      @@
@@  Die Reihe ist an dir!  @@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

'''
start = '''
===========================
Bitte drei Boote setzen:
ein 1er-Boot, ein 2er-Boot
und ein 3er-Boot.
Bsp.: a1 | c3:d3 | e6:f6:g6
===========================
'''

shot = '''

---------------------------
Du bist dran mit Schiessen!
---------------------------

'''

vspace = '\n\n\n\n\n\n\n\n\n\n\n\n' #Abstandhalter

win = '''
##################################
#                                #
#  *** GRATULATION ZUM SIEG ***  #
#                                #
##################################'''

######## Konstanten ##########
O = '~' # wasser
T = 'X' # versenkt
F = 'o' # Fehlschuss
V = '%' # versenkt

#### Spielfeldvariablen, für grösseres Spielfeld spalte und reihe anpassen ####
#spalte = list('ABCDEFGHIJ')
#reihe = range(1,11)
#empty_field = []


##### Funktionen #####
reihe = range(1,11) #diese Variable bestimmt die Spielfeldgrösse
alpha = 'abcdefghijklmnopqrstuvwxyz'
spalte = list(alpha[:len(reihe)].upper()) #für die Spaltenüberschrift, und iterationen in alten Funktionen
spalte_nr = list(enumerate(alpha[:len(reihe)])) # für type_check(), nummerierte spalten für listenindices
inputa = 'c5-c9|a1-a4|f8-i8|h8-h10|a4-a6|b2-b4|e5-e6|d3-e3|h1-h2|g5-g6' #testpositionen


def type_check(boat, bclass):
    boote = []
    boatpos = []
    btype = []
    
    if boat[0][0] == boat[1][0]:#horizontal gesetztes boot
        btype = ['v', 1 + int(boat[1][1:])-int(boat[0][1:])] #bestimmt bootstyp in liste: vertikal oder horizontal, laenge
        for i in range(btype[1]):
            if btype[1] == bclass:
                boatpos.append([[boat[0][0].upper(), int(boat[0][1:]) + i], btype[1]]) #
            else:
                print("Boot zu kurz oder zu lang eingegeben!")
                return False, False
    elif boat[1][1:] == boat[0][1:]: #vertikal gesetztes boot
        for i, bst in spalte_nr:
            if bst == (boat[1][0]):
                end_index = i
            elif bst == (boat[0][0]):
                start_index = i
        btype = ['h', 1 + end_index-start_index]
        for i in range(btype[1]):
            if btype[1] == bclass:
                boatpos.append([[spalte_nr[start_index+i][1].upper(), int(boat[0][1:])], btype[1]])
            else:
                print("Boot zu kurz oder zu lang eingegeben!")
                return False, False
    elif (boat[0][0] != boat[1][0] and boat[1][1:] != boat[0][1:]):
        print("Diagonale Platzierung der Boote nicht erlaubt!")
        return False, False

    #print(boatpos)
    return btype, boatpos
#type_check(inputa)

def boot_input(field):
##    1-mal typ 5 #schlachtshiff
##    2-mal typ 4 #kreuzer
##    3-mal typ 3 #zerstörer
##    4-mal typ 2 #u-boot
    print('''
Muster zum Boote setzen:
Für 3 Zerstörer, alle in einer Eingabe:
Eingabebeispiel: c3-c5|g4-i4|h1-h3

(Pipesymbol auf Mac: Alt+Shift+7)
''')
    boats = []
    
    boatclass = [[5, 'Schlachtschiff'], [4, 'Kreuzer'], [3, 'Zerstörer'], [2, 'U-Boote']]
   # for i, value in enumerate(boatclass):
    #klasse = [value[0] for value in boatclass] #list comprehension für nummerierung der bootsklassen

    for i in range(0,4):
        show_playfield(make_field(field, boats))
        while True:
            eingabe= input(str(i+1) + ' mal ' + boatclass[i][1] + ' setzen; \n(' + str(boatclass[i][0]) + ' Felder, horizontal oder vertikal): ')
            #eingabe = inputa
            
            try:
                if '|' in eingabe:
                    bclasslist = []
                    bootsklasse = eingabe.split('|')
                    if len(bootsklasse) != i+1:
                        print("Zu wenig Boote gesetzt!")
                        raise Exception
                    #print(bootsklasse)
                    for boot in bootsklasse:
                        boot = str(boot).split('-')
                        bclass = type_check(boot, boatclass[i][0])
                        bclasslist.extend(bclass[1])
                    #print("DEBUG: vor if len(bclasslist)", bclasslist)                    
                    if len(bclasslist) == (i+1)*boatclass[i][0]:
                        for pos, x in bclasslist:
                            bpos = [value[0] for value in boats]
                            bclasslistpos = [value[0] for value in bclasslist]
#                            print("bpos:", bpos)
                            # in der übergebenen Liste doppelte positionen ausschliessen (in boats und in bclasslist)
                            if pos not in bpos and bclasslistpos.count(pos) == 1: 
#                                print("pos OK:", pos)
                                continue
                                
                            else:
                                print("Da liegt schon ein Boot!")
                                bclasslist = []
                                raise Exception
                            
                        boats.extend(bclasslist)
                                
                    else:
                        bclasslist = []
                        print("Da ist was falsch!")
                        raise Exception
#                    print(len(bclasslist), (i+1)*boatclass[i][0], len(boats))        
#                    print("Debug: nach listenerstellung split pipe: ", boats, len(boats))
                
                else:
                    bootsklasse = eingabe.split('-')
                    bclass = type_check(bootsklasse, boatclass[i][0])
                    boats.extend(bclass[1])
                 #   print("Debug: einzelboot, das schlachtschiff, bei eingabe user", boats, len(boats))
                  #  print(len(boats), (i+1)*boatclass[i][0])        
                break
            except Exception:
                print("Bootfehler!")
            except ValueError:
                print("Bitte Boote wie im Muster angegeben eingeben.")
            except TypeError:
                print("Bitte Boote wie im Muster angegeben eingeben.")                
#   # print(boats) 
    return boats

#macht ein leeres Spielfeld ohne Boote, also alle Einträge sind spielfeld[[bst, i], O]
def empty_playfield():
    empty_field = []
    for i in reihe:
        zeile = str(i)
        for x, bst in enumerate(spalte):
            empty_field.append([[bst, i], O])
            zeile = zeile + ' ' + empty_field[x][1]
        #print(zeile)
    return empty_field #übergibt das leere Spielfeld an das Spiel

# gibt ein Spielfeld aus
def show_playfield(playfield):
    #schleife für die Kopfzeile
    matrixhead = '  '
    buffer = ''
    if len(reihe) > 9:
        buffer = '0'
    for bst in spalte:
        matrixhead = matrixhead + ' '  + bst
    print(matrixhead) #Spaltenbuchstaben des Spielfelds
    #schleife für die Matrix
    for i in reihe:
        zeile = buffer + str(i)
        if i > 9:
            zeile = str(i)
        for x, bst in enumerate(spalte):
            zeile = zeile + ' '  + str(playfield[x+(i-1)*len(reihe)][1])
        print(zeile)
    return


# Schiessen
def shoot():
    #print(shot)
    while True:
        target = input('---------------\nWähle ein Ziel: ').upper()
        print("---------------")
        time.sleep(.5)
        #zielposition auswerten
        try:
            pos = [target[0],int(target[1:])]
            return pos
        except ValueError:
            print("Ziel falsch eingegeben, nochmal!")
            print("********************************")
            time.sleep(0.2)

# zentrale Spiellogik, auf Treffer prüfen und Trefferlog führen
def logic(sq_field, trefferlog, target):
    field = []
    treffer = False
    victory = False
    for i, value in trefferlog:
        if i == target:  # Wenn das Ziel im log notiert ist, Fallunterscheidung
            # versenkt, wenn auf position -1 oder +1 keine zahl 1, 2-er 2, 3er 3, neu mit 5, 4, 3, 2
##            if value in [2,3,4,5] and trefferlog[trefferlog.index([i, value]-1)][1] == T:
##                trefferlog[trefferlog.index([i, value])][1] = V  # Eintragen neuer Wert
##                field = sqfield(sq_field, trefferlog)
##                show_playfield(field)
##                print('Boot versenkt!')
##                treffer = True
##                #gewonnen?
##                victory = check_win(trefferlog)
##                return field, trefferlog, treffer, victory

            if value == 2:
                #Zähler für Bootspositionen
                status = [value[1] for value in trefferlog]  #abfrage über list comprehension
                if status.count(2) > 1:
                    trefferlog[trefferlog.index([i, value])][1] = T # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Treffer!')
          
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer, victory
                # als versenkt loggen, wenn es der letzte Bootsbaustein ist, geht mit mehreren Schiffen nicht mehr
                else:
                    trefferlog[trefferlog.index([i, value])][1] = V  # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Boot versenkt!')
    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer, victory

            if value == 3:
                status = [value[1] for value in trefferlog] #abfrage über list comprehension
                if status.count(3) > 1:
                    trefferlog[trefferlog.index([i, value])][1] = T # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Treffer!')
    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer, victory
                # als versenkt loggen, wenn es der letzte Bootsbaustein ist
                else:
                    trefferlog[trefferlog.index([i, value])][1] = V # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Boot versenkt!')
    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer,  victory
            if value == 4:
                status = [value[1] for value in trefferlog] #abfrage über list comprehension
                if status.count(4) > 1:
                    trefferlog[trefferlog.index([i, value])][1] = T # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Treffer!')
    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer, victory
                # als versenkt loggen, wenn es der letzte Bootsbaustein ist
                else:
                    trefferlog[trefferlog.index([i, value])][1] = V # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Boot versenkt!')
    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer,  victory
            if value == 5:
                status = [value[1] for value in trefferlog] #abfrage über list comprehension
                if status.count(5) > 1:
                    trefferlog[trefferlog.index([i, value])][1] = T # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Treffer!')
                    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer, victory
                # als versenkt loggen, wenn es der letzte Bootsbaustein ist
                else:
                    trefferlog[trefferlog.index([i, value])][1] = V # Eintragen neuer Wert
                    field = sqfield(sq_field, trefferlog)
                    show_playfield(field)
                    print('Boot versenkt!')
                    
                    treffer = True
                    victory = check_win(trefferlog)
                    return field, trefferlog, treffer,  victory
                
            if value == T or value == F or value == V:
                trefferlog.append([target, F]) # Fehlschuss im log eintragen    
                field = sqfield(sq_field, trefferlog) # Fehlschuss im Feld eintragen
                show_playfield(field)
                print('Da hast du schon einmal hingeschossen, du Eumel!')

                return field, trefferlog, treffer, victory

    # wenn nix im log steht, Fehlschuss notieren    
    trefferlog.append([target, F]) # Fehlschuss im log und auf dem Spielfeld eintragen
    field = sqfield(sq_field, trefferlog)[:]
    show_playfield(field) #spielfeld mit Fehlschuss anzeigen
    print('\n--------\nDANEBEN!\n--------\n')

    #print("Debug: in logic():", trefferlog) # für Testläufe, zeigt Boots/trefferliste
    return field, trefferlog, treffer, victory #übergibt das leere Spielfeld an das Spiel

# prüfen ob gewonnen, angesteuert von logic() nach jeder trefferlog-aenderung
def check_win(sl):
    status = [value[1] for value in sl]
    for i in status:
        if i in [2, 3, 4, 5]:
            #print('nonig')
            return False
        continue
    #print('guunnääää')
    return True

# Funktion für den Spielzug in logic(), läuft durch das Status-quo-Spielfeld und setzt hits&misses
def sqfield(field, ledger): 
    for i, entry in enumerate(field):
        for index in ledger:
            if entry[0] == index[0]:
                if index[1] not in [2, 3, 4, 5]:
                    field[i][1] = index[1]
    
    return field

# setzt boote auf das spielfeld und zeigt sie, zu Debug-Zwecken, vom Spiel derzeit nicht verwendet
def make_field(spielfeld, boats): 
    field = []
    for i, bst in spielfeld:
        for p, pos in boats:
            if i == p:
                bst = pos
        field.append([i, bst])
    #print(field)
    return field

# Spieldurchgang pro Spieler; bis gewonnen oder Fehlschuss
def play(empty_field, log, turn):
    treffer = True
    victory = False
    quote = 0
    counter = 0
    hitcount = 0
    newlog = log[:]
    #print("DEBUG log nextplayer: play, log:" , newlog)
    while treffer and victory == False:
        empty_field = empty_playfield()
        print(turn)
        show_playfield(sqfield(empty_field, newlog))    #show_playfield(make_field(empty, boote)) #zum Debuggen boote anzeigen, empty_field für leeres Feld
#        print(5*vspace)
        time.sleep(.5)         
        feld, status, treffer, victory = logic(empty_field, newlog, shoot())  #rückgabewerte in Tupel mit Feld und Boot/Trefferlog
        #print("DEBUG: rückgabe von logic:", status) # zum Testen
        counter +=  1
        if treffer:
            hitcount+= 1
        quote = hitcount/counter
        newlog = status[:]
        print('Du hast in dieser Runde %d mal geschossen! Deine Trefferquote beträgt %.0f%%.' % (counter, quote*100))
#        print(5*vspace)
        time.sleep(5)         
        print(5*vspace)
#        time.sleep(1)
    return newlog, counter, hitcount, victory

#menüpunkt1
### für zwei spieler
def battle ():
    spieler1 = []
    spieler2 = []
    ledger1 = []
    ledger2 = []
    count1 = 0
    count2 = 0
    hits1 = 0
    hits2 = 0
    empty_field = empty_playfield()
    print(eins)
    ledger1 = boot_input(empty_field)#neu# boot_input() statt set_boats() # boote, zum Testen
    #show_playfield(make_field(empty_field, boote))
   # print(10*vspace)
    print(7*vspace)   
    print(zwei)
    ledger2 = boot_input(empty_field)#neu# boot_input() statt set_boats() #set_boats() # boote2, zum Testen
    #show_playfield(make_field(empty_field, boote2))
    print(7*vspace)
    
    while True:
        print(7*vspace)
        #print(eins)
        #show_playfield(sqfield(empty_field, ledger2))
        spieler1 = play(empty_field, ledger2, eins)
        ledger2 = spieler1[0][:]
        print(7*vspace)
        #print(zwei)
        spieler2 = play(empty_field, ledger1, zwei)
        ledger1 = spieler2[0][:]
        #spielstatistik
        count1 = count1 + spieler1[1]
        count2 = count2 + spieler2[1]
        hits1 = hits1 + spieler1[2]
        hits2 = hits2 + spieler2[2]
        # Abbruchbedingung: gewinner
        if spieler1[3] or spieler2[3]:
            print(win)
            print ('Spieler eins hat insgesamt %d Mal geschossen und %d Mal getroffen.\nSpieler zwei hat %d Mal geschossen und %d Mal getroffen.' %(count1, hits1, count2, hits2))
            time.sleep(5)
            return

#Menueaufbau
def menu():
        
    print("""
======================================================
Hallo Professor Falken. Möchten Sie ein Spiel spielen?
  1 ... Battleships 2 Spieler
  2 ... Battleships Computer gegen 1 Spieler
  3 ... Battleships 1 Spieler gegen Computer
  4 ... Weltweiter thermonuklearer Krieg
  99... Beenden
======================================================""")

    choice = (input("Gewählte Option: "))
    if choice.isdigit():
        choice = int(choice)
    return choice

def untermenu(): #einfaches untermenu
    subchoice = input("(" + "Boote setzen"[0] + ")" + "Boote setzen"[1:] + ", " + "(" + "zufällig setzen"[0] + ")" + "zufällig setzen"[1:] + " oder " + "zurück zum Menü mit X" + "? ")
    return subchoice



#menüpunkt 2
def funktion2(argumente):
    #tue etwas
    print(argumente)# Ausgabe zu debugging-zwecken, prüfen, was ich tue
    
    #anweisungen, verzweigungen, bedingungen, variablen
    return  #Rückgabewert oder zurück zur Programmführung

#menüpunkt 3
def funktion3(argumente):
    print(argumente)# Ausgabe zu debugging-zwecken, prüfen, was ich tue

    #tue etwas, mit untermenu
    trigger = untermenu().lower() #Rückgabewert aus dem Untermenu an Variable übergeben
    
    #case 1 - löschen
    if trigger == 'b':
        print("Bitte Boote setzen.")
        #hier setzt der Spieler die Boote, player vs. py
        return             
    
    #case 2 - korrigieren
    if trigger == 'z':
        print("Py setzt die Boote.")
        #py setzt Boote, py vs. player
        return 

#Programmsteuerung
def main():
    wahl = 0
    while wahl != 4:
            
        wahl = menu()
        if wahl == 1:
            battle()#argumente nur, wenn in der Funktion welche definiert wurden
        
        elif wahl == 2:
            func2 = "Leider noch nicht funktional, probier es später wieder."
            funktion2(func2) #argumente nur, wenn in der Funktion welche definiert wurden
        
        elif wahl == 3:
            func3 = "Kommt demnächst."
            funktion3(func3) #argumente nur, wenn in der Funktion welche definiert wurden
        
        elif wahl == 4:
            print('''Das ist ein seltsames Spiel Professor Falken:\n
Der einzige gewinnende Zug ist, NICHT zu spielen...''')
            
        elif wahl == 99:
            return  #argumente nur, wenn in der Funktion welche definiert wurden

##### hier fängt alles an, mal noch ohne main
##main()
battle() #ohne menü für tests

