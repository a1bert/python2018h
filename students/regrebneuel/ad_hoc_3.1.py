#adhoc uebungen 3.1
#3.1.a
sterntext = input("Bitte Text eingeben: ")
print(sterntext)
for no, bst in enumerate(sterntext):
    sterntext = sterntext.replace(bst, "*")
    
print(sterntext)    

#3.1.b
text2parse = input("Bitte Text eingeben: ").lower()
char = input("Bitte gesuchtes Zeichen eingeben: ").lower()
result = ""
for pos, elem in enumerate(text2parse):
    if elem == char:
        result = result + str(pos) + " "
print(result)        


#3.1.c
text = "Das Leben ist grausam und schrecklich gemein."
print(text.replace(" ",""))

neutext = input("Bitte Text eingeben: ")
print(neutext.replace(" ",""))
