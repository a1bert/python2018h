# ad_hoc_2.9b
#zeichnen mit turtle Grafikbibliothek
from turtle import *
speed(10)

# dreieck als funktion
def dreieck(sl): #ein blaues dreieck mit seitenlaenge als argument
    begin_fill()
    pencolor("blue")
    fillcolor("blue")
    for i in range(3):
        fd(sl)
        lt(120)
    end_fill()
    
anzahl = int(input("Wie viele Dreiecke hÃ¤tten's denn gern? "))
groesse = int(input("Und wie gross? "))


penup(); back(anzahl * (groesse) / 2); pendown()
startpunkt = position()

for d in range(anzahl):
    if d ==0 or d % 2 == 1: #
        dreieck(groesse)
        penup()
        fd(groesse + groesse * 0.2)
        pendown()
    else:
        penup()
        goto(startpunkt); rt(90); fd(groesse + groesse * 0.2); lt(90) #in nÃ¤chste Reihe wechseln
        pendown()
        startpunkt = position()
        dreieck(groesse)
        penup(); fd(groesse + groesse * 0.2); pendown()
        
hideturtle()
    
exitonclick()



