# 2.7c Turtlespirale
from turtle import *

pensize(3); pencolor("red"); speed(40)
penup(); goto(0,-200); pendown()

radius = 200
for i in range(11):
    circle(radius, 180)
    radius = radius - 20
hideturtle()    
exitonclick()    