# Muster mit Loops

ausgabe = ""

# (a)
for i in range (8):
    ausgabe = ausgabe + '#'
    print(ausgabe)
    
print("========")    

# (b)
ausgabe = 8*"#"

for i in range (8, 0, -1):
    ausgabe = ausgabe[0:i]
    print(ausgabe)
##    i -=1
    
print("========")

# (c)
ausgabe = 8*"#"
for i in range (8):
    print(ausgabe)
    ausgabe = " " + ausgabe[0:-1]
    
print("========")    
# (d)
ausgabe = "       #"
for i in range (8):
    print(ausgabe[i:])
    ausgabe = ausgabe + "#"

print("========")    

# (e)
ausgabe = "#######"
for i in range (7):
    if 0 < i < 6:
        ausgabe2 = (ausgabe[0] + (len(ausgabe)-2)*" " + ausgabe[-1])
        print(ausgabe2)  
    else:
        print(ausgabe)

print("========")    

# (f)
ausgabe = "#######"
for i in range (7):
    if 0 < i < 6:
        ausgabe2 = (i*" " + "#" + (len(ausgabe)-2*i)*" ")
        print(ausgabe2)  
    else:
        print(ausgabe)

print("========")    

# (g)
ausgabe = "#######"
for i in range (6,-1,-1):
    if 0 < i < 6:
        ausgabe2 = (i*" " + "#" + (len(ausgabe)-2*i)*" ")
        print(ausgabe2)  
    else:
        print(ausgabe)

print("========")    
# (h)
ausgabe = "#######"
for i in range (7):
    if 0 < i < 6:
        if i == 1 or i == 5:
            ausgabe2 = " " + ausgabe[1] + "   " + ausgabe[-2] + " "
            print(ausgabe2)
        elif i == 2 or i == 4:
            ausgabe3 = "  " + ausgabe[2] + " " + ausgabe[4] + " "
            print(ausgabe3)
        else:
            ausgabe4 = "   " + ausgabe[3] + "   "
            print(ausgabe4)
    else:
        print(ausgabe)

print("========")    
# (h)
ausgabe = "#######"
for i in range (7):
    if 0 < i < 6:
        if i == 1 or i == 5:
            ausgabe2 = ausgabe[0:2] + "   " + ausgabe[-1:-3:-1]
            print(ausgabe2)
        elif i == 2 or i == 4:
            ausgabe3 = ausgabe[0 ]+ " " + ausgabe[2] + " " + ausgabe[4] + " " + ausgabe[6]
            print(ausgabe3)
        else:
            ausgabe4 = "#  " + ausgabe[3] + "  #"
            print(ausgabe4)
    else:
        print(ausgabe)
