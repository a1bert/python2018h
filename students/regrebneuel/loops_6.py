#zeichne Rahmen
from turtle import *
speed(40)
def zeichne_rahmen(aussen, innen, stiftfarbe, fuellfarbe):
    fillcolor(fuellfarbe)
    pencolor(stiftfarbe)
    begin_fill()
    for i in range(4):
        fd(aussen)
        lt(90)
    penup()
    fd(innen)
    lt(90)
    fd(innen)
    rt(90)
    pendown()
    for i in range(4):
        fd(aussen-2*innen)
        lt(90)
    end_fill()
    
    hideturtle()
    exitonclick()
    
zeichne_rahmen(100, 25, 'black', 'white')
               
