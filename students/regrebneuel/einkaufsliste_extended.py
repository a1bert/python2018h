#Einkaufsliste: item oder x fÃ¼r einkaufsliste (Apfel, Birne, Kirsche, Banane) abfragen, gibt Einkaufsliste aus

ekliste = []

#menÃ¼punkt1
def items_eingeben(eingabe):
    print("--------------------------")
    while eingabe != 'x':
        eingabe = input("Item: ")
        if eingabe == 'x':
            break
        ekliste.append(eingabe)
    #print("\n")        
    return main()

#menÃ¼punk3
def items_ausgeben():
    ekliste.sort()
    print("""
==============
Einkaufsliste:
--------------""")
    if ekliste == []:
        print("""
         Die Liste ist noch leer!
        Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°""")
        return main()
    
    for item in ekliste:
        print(item)
    print("==============\n")
    return main()

#items prÃ¼fen Liste ausgeben mit MenÃ¼ fÃ¼r Korrektur
#mit 0 fÃ¼r Exit ins HauptmenÃ¼
# 2 (l)Ã¶schen oder (k)orrigieren
def items_korrigieren():
    trigger = input("(" + "lÃ¶schen"[0] + ")" + "lÃ¶schen"[1:] + ", " + "(" + "korrigieren"[0] + ")" + "korrigieren"[1:] + " oder " + "beenden mit 0" + "? ")
    
    #case 1 - lÃ¶schen
    if trigger == 'l':
        sel = ""
        while sel != 0:
            itemno = list(enumerate(ekliste))
            print("==============\n")
            for no in itemno:
                print(no[0]+1, no[1])
            print("==============")
            sel = int(input("Welches Element mÃ¶chten Sie lÃ¶schen? "))
            if sel != 0:
                del ekliste[sel-1]
        return main()            
    
    #case 2 - korrigieren
    if trigger == 'k':
        sel = ""
        while sel != 0:
            itemno = list(enumerate(ekliste))
            print("==============")
            for no in itemno:
                print(no[0]+1, no[1])
            print("==============")
            sel = int(input("Welches Element mÃ¶chten Sie korrigieren? "))
            if sel != 0:
                ekliste[sel-1] = input("Korrektur: ")
        return main()
    
    if trigger == 'b' or trigger == '0':
        return main()

def main():
    wahl = menu()
    if wahl == 1:
              items_eingeben("")
    elif wahl == 2:
              items_korrigieren()
    elif wahl == 3:
              items_ausgeben()
    elif wahl == 4:
        return print("""
 ========================
||    Programm beendet   ||
 ========================""")
    
    else:
              print("ungÃ¼ltige Wahl!")
              print("::::nochmal::::")
              return main()
def menu():
    print("""
-------------------------
MenÃ¼:
    1. ... Items eingeben
    2. ... Items prÃ¼fen
    3. ... Items ausgeben
    4. ... Exit
--------------------------""")
    choice = int(input("GewÃ¤hlte Option: "))
    return choice

main()          
