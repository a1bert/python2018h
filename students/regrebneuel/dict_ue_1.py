#dict uebung 1
'''
Zeichenstatistik: Schreiben Sie eine Funktion, welche zu einem Text
alle Buchstaben und deren HÃ¤ufigkeit ausgibt.
'''

l_input = "guten morgen!"

def letter_stat(input):
    d = {}
    l = list(input)
    for bst in l:
        if bst in d:
            continue
        d[bst] = l.count(bst)
    print(d)
    return d
    
letter_stat(l_input)