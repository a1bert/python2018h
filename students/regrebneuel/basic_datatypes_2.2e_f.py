#basic datatypes Ã¼bung 2.2 b/e/f Ã¼ber MenÃ¼-Auswahl mit Eingabe von 2.1f

preisliste = {}

#menÃ¼punkt1
def items_eingeben(lm):
    print("--------------------------")
    preise ={}
    while lm != 'x':
     
        lm = input("Lebensmittel: ")
        if lm == 'x':
            break        
        preis = float(input("Preis: "))
        preise[lm] = preis

    print(preise)
    return preise

#menÃ¼punkt2
def items_filter(pl, suffix):
    output = []
    
    for word in pl.keys():
        if word[-len(suffix):] == suffix: #das Ende des Wortes prÃ¼fen auf Gleichheit mit suffix via wort von hinten auf lÃ¤nge des suffix
            print(word)
            output.append(word)
    return output

   

#menÃ¼unterpunkt 3a
def items_ausgeben(pl):
    print("""
===========
Preisliste:
-----------""")
    if pl == {}:
        print("""
         Die Liste ist noch leer!
        Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°Â°""")
        return pl
    
    for lm, preis in sorted(pl.items()):
        print("%s kostet %.2f CHF." % (lm, preis))
    print("====================================")
    return pl

# menÃ¼punkt3
def items_ordnen(pl):
    trigger = input("Sortieren nach " + "(" + "Preis"[0] + ")"  + "Preis < 2.20 CHF"[1:] + " oder" +  "\n" + "Nach (" + "Lebensmittel"[0] + ")" + "Lebensmittel"[1:] + " oder \n" + "beenden mit 0" + "? ").lower()
    
    #case 1 - Nach Preis sortiert ausgeben mit extraschleife
    if trigger == 'p':
        l = []
        for lm, pr in pl.items():
            l.append([pr, lm])
            l.sort()
        print("""
===========
Preisliste:
-----------""")
        for pr, lm in l:
            if pr < 2.2:
                print("%.2f CHF fÃ¼r  %s." % (pr, lm))
        print("----------------------------------")
        return#funktion fÃ¼r sortieren nach Preis
    
    #case 2 nach key sortiert ausgeben mit eigener Funktion
    if trigger == 'l':
        sorted = items_ausgeben(pl)
        return
        #funktion fÃ¼r Filtern nach -en
    # case 3: abbruch    
    if trigger == 'b' or trigger == '0':
        return 

def main(): #nicht mehr rekursiv
    wahl = 0
    while wahl != 4:
        wahl = menu()
        if wahl == 1:
                  preisliste = items_eingeben("")
                  
        elif wahl == 2:
                  filter = items_filter(preisliste, 'en')
        elif wahl == 3:
                  items_ordnen(preisliste)
        elif wahl == 4:
            return print("""
        # # # # # # # # # # # # #
        #    Programm beendet   #
        # # # # # # # # # # # # #""")

        else:
                  print("ungÃ¼ltige Wahl!")
                  print("::::nochmal::::")
              
def menu():
    print("""
-------------------------
MenÃ¼:
    1. ... Items eingeben
    2. ... Items Filtern
    3. ... Items sortiert ausgeben
    4. ... Exit
--------------------------""")
    choice = int(input("GewÃ¤hlte Option: "))
    return choice

main()          
