# mit for (vorübung1):
for zeichen in range(7):
    print("# ")
    
# 
# 
# 
# 
# 
# 
#

# (vorübung2)
print() # macht Zeilenabstand
for zeichen in range(7):
        print("# " * 7)

# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # #


# mit for:
print()
for zeichen in range(7):
    if zeichen == 0 or zeichen == 6:
        print("# " * 7)
    else:
        print("#" + " " * 11 + "#")
print(" " * 5 + "(e)")

# # # # # # # 
#           #
#           #
#           #
#           #
#           #
# # # # # # # 
#    (e)



# mit while (vorübung):
print() # macht Zeilenabstand
zeichen = 1
while zeichen <= 7:
        print("# " * 7)
        zeichen = zeichen + 1

# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # # 
# # # # # # #


# mit while:
print()
zeichen = 1
while zeichen <= 7:
    if zeichen == 1 or zeichen == 7:
        print("# " * 7)
    else:
        print("#" + " " * 11 + "#")
    zeichen = zeichen + 1
print(" " * 5 + "(e)")

# # # # # # # 
#           #
#           #
#           #
#           #
#           #
# # # # # # # 
#    (e)