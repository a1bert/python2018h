print("==================================================")
print("      Programmubersicht:")
print("1 ... Preis fur eine Fahrkarte berechnen")
print("2 ... Eintritt fur das Schwimmbad berechnen")
print("3 ... Mahngebuhr fur die Bibliothek berechnen")
print("0 ... Programm beenden")
print("==================================================")
x = int(input("Bitte wahlen Sie eine Option aus: "))

while x != 0:
    if x == 1:
        alter = int(input("Bitte geben Sie Ihr Alter ein: "))
        entfernung = int(input("Bitte geben Sie die Entfernung in km ein: "))
        def ticket():
            if alter < 16 and alter >= 6:
                preis = ((entfernung * 0.25) + 2) / 2
                return "Preis: " + str(preis) + " CHF"
            elif alter < 6:
                preis = 0
                return "Preis: " + str(preis) + " CHF"
            else:
                preis = (entfernung * 0.25) + 2
                return "Preis: " + str(preis) + " CHF"
            
        print(ticket())
        z = input("Weiter mit Enter")
        print("==================================================")
        print("      Programmubersicht:")
        print("1 ... Preis fur eine Fahrkarte berechnen")
        print("2 ... Eintritt fur das Schwimmbad berechnen")
        print("3 ... Mahngebuhr fur die Bibliothek berechnen")
        print("0 ... Programm beenden")
        print("==================================================")
        x = int(input("Bitte wahlen Sie eine Option aus: "))

    elif x == 2:
        karte = input("Bitte geben Sie die gewunschte Eintrittskarte ein (kurzzeitkarte / nachmittagskarte / tageskarte): ")
        alter = int(input("Bitte geben Sie Ihr Alter an: "))
        def eintritt():
            if karte == "kurzzeitkarte":
                preis = 5
            elif karte == "nachmittagskarte":
                preis = 6
            elif karte == "tageskarte":
                preis = 10
            if alter <= 6:
                return "gratis Eintritt"
            elif alter > 6 and alter <= 12:
                return "Der Eintritt kostet " + str(preis / 2) + " CHF"
            elif alter > 12:
                return "Der Eintritt kostet " + str(preis) + " CHF"
        
        print(eintritt())
        z = input("Weiter mit Enter")
        print("==================================================")
        print("      Programmubersicht:")
        print("1 ... Preis fur eine Fahrkarte berechnen")
        print("2 ... Eintritt fur das Schwimmbad berechnen")
        print("3 ... Mahngebuhr fur die Bibliothek berechnen")
        print("0 ... Programm beenden")
        print("==================================================")
        x = int(input("Bitte wahlen Sie eine Option aus: "))


    elif x == 3:
        anzahl_medien = int(input("Anzahl ausgeliehene Medien: "))
        anzahl_mahnungen = int(input("Anzahl Mahnungen: "))
        rechnungsversand = input("GewÃ¼nschte Versandart (mailversand / postversand): ")
        def mahngebuehr():
            if anzahl_mahnungen == 1:
                kosten = 2
                return "Es fallen keine Gebuhren an"
            elif anzahl_mahnungen == 2:
                kosten = 4
                return "Es fallen keine Gebuhren an"
            elif anzahl_mahnungen >= 3:
                if rechnungsversand == "mailversand":
                    kosten = 6
                elif rechnungsversand == "postversand":
                    kosten = (anzahl_mahnungen - 2) * 2 + 6
                return "Gebuhren: " + str(anzahl_medien * kosten) + " CHF"

        print(mahngebuehr())
        z = input("Weiter mit Enter")
        print("==================================================")
        print("      Programmubersicht:")
        print("1 ... Preis fur eine Fahrkarte berechnen")
        print("2 ... Eintritt fur das Schwimmbad berechnen")
        print("3 ... Mahngebuhr fur die Bibliothek berechnen")
        print("0 ... Programm beenden")
        print("==================================================")
        x = int(input("Bitte wahlen Sie eine Option aus: "))
        
    else:
        x = int(input("Bitte wahlen Sie eine gultige Option aus: "))

print("ENDE")