a = int(input("Bitte geben Sie den 1. Preis ein: "))

price = 0

z = 1

while a != "x":
    a = int(a)
    price = a + price
    z = z + 1
    a = input("Bitte geben Sie den " + str(z) + ". Preis ein: ")
    
price = int(price)

if price < 100:
    ausgabe = "Kein Rabatt, Gesamtpreis = " + str(price) + " CHF"
elif price >= 100 and price < 1000:
    ausgabe = "5% Rabatt, Gesamtpreis = " + str(price * 0.95) + " CHF"
elif price >= 1000:
    ausgabe = "10% Rabatt, Gesamtpreis = " + str(price * 0.9) + " CHF"

print(ausgabe)