#Übungsbeispiele Schwerpunkt Funktionen und Bedingungen
#Tamara Mächler

def berechnet_eintrittspreis (a,b,c):
    if a == "tageskarte":
        if b == "false":
            if c <= 6:
                preis = 0
                print(preis)
            elif c in range(7,13):
                preis = 10/2
                print(preis)
            elif c > 12:
                preis = 10
                print(preis)
        elif b == "true":
             if c <= 6:
                preis = 0
                print(preis)
             elif c in range(7,13):
                preis = (10/2)/100*90
                print(preis)
             elif c > 12:
                preis = 10/100*90
                print(preis)
    elif a == "nachmittagskarte":
        if b == "false":
            if c <= 6:
                preis = 0
                print(preis)
            elif c in range(7,13):
                preis = 6/2
                print(preis)
            elif c > 12:
                preis = 6
                print(preis)
        elif b == "true":
             if c <= 6:
                preis = 0
                print(preis)
             elif c in range(7,13):
                preis = (6/2)/100*90
                print(preis)
             elif c > 12:
                preis = 6/100*90
                print(preis)
    elif a == "kurzzeitkarte":
        if b == "false":
            if c <= 6:
                preis = 0
                print(preis)
            elif c in range(7,13):
                preis = 5/2
                print(preis)
            elif c > 12:
                preis = 5
                print(preis)
        elif b == "true":
             if c <= 6:
                preis = 0
                print(preis)
             elif c in range(7,13):
                preis = (5/2)/100*90
                print(preis)
             elif c > 12:
                preis = 5/100*90
                print(preis)
                
berechnet_eintrittspreis("kurzzeitkarte", "false", 30)
berechnet_eintrittspreis("tageskarte", "false", 2)
berechnet_eintrittspreis("tageskarte", "true", 12)
        
