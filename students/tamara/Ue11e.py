#Übungsbeispiele Schwerpunkt Funktionen und Bedingungen 11e
#Tamara Mächler

b=1

def spass_e(b):
    if b == 1:
        print ("# " *7)
    for b in range(2,7):
        print ("# " + " " *10 + "#") 
    else:
        print ("# " *7)

spass_e(b)
    