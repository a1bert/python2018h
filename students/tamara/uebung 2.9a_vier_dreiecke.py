# ad hoc Übung 2.9a dreiecke
# Tamara Mächler

from turtle import *

reset()

def dreieck():
    fillcolor("cornflowerblue")
    begin_fill()
    forward(50)
    left(120)
    forward(50)
    left(120)
    forward(50)
    left(120)
    end_fill()
    penup()
    forward(100)
    pendown()
    hideturtle()    
    
for i in range (4):
    print(dreieck())
    
exitonclick()
    