
try:
    with open ("data.py", "r") as originaldatei:
        with open ("data_kopie.py", "w") as sicherungskopie:
            n = 1
            for zeile in originaldatei:
                sicherungskopie.write(str(n) + ": " + zeile)
                n = n + 1
except FileNotFoundError:
    print("Datei konnte nicht geöffnet werden. Existiert diese?")

except PermissionError:
    print("Datei konnte nicht geschrieben werden. Haben Sie die Berechtigung?")
            