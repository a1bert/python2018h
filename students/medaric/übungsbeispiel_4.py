## 4.
def berechnet_eintrittspreis(karte, ermaessigung, alter):
        
    if karte == "kurzzeitkarte":
        x = 5
        
        if ermaessigung == True:
            x = x * 0.9
        
            if alter < 6:
                x = 0
            elif alter <= 12:
                x = x/2
            
    
    elif karte == "nachmittagskarte":
        x = 6
        if ermaessigung == True:
            x = x * 0.9
        
            if alter < 6:
                x = 0
            elif alter <= 12:
                x = x/2
    
    elif karte == "tageskarte":
        x = 10
        
        if ermaessigung == True:
            x = x * 0.9
            
            if alter < 6:
                x = 0
            elif alter <= 12:
                x = x/2
                
    preis = x
            
    return preis

print(berechnet_eintrittspreis('kurzzeitkarte', False, 30))
print(berechnet_eintrittspreis('tageskarte', False, 2))
print(berechnet_eintrittspreis('tageskarte', True, 12))
print(berechnet_eintrittspreis('kurzzeitkarte', True, 30))

