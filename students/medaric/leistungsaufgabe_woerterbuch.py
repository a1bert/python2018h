## Leistungsaufgabe 3. b

# a. Erstellen Sie mittels einem Dictionary (dict) ein
# Mini-Deutsch-Englisch WÃ¶rterbuch (10 Vokabeln).
# Ein zugehÃ¶rige Vokabeltrainer soll alle Vokabeln nach folgendem Muster abfragen:

dict = {"Haus":"house", "Tochter":"daughter", "Hund":"dog", "Flasche":"bottle",
        "Bruder":"brother", "Tisch":"table", "Stuhl":"chair", "Wasser":"water",
        "KÃ¤se":"cheese", "Bett":"bed"}

points = 0
for deutsch, englisch in dict.items():
    eingabe = input("Was heisst " + str(deutsch)+ "? ")
    points = int()
        
    if dict[deutsch] == eingabe:
        eingabe = True
        print("Richtig!")
        points = points + 1
        
    elif dict[deutsch] != eingabe:
        eingabe = False
        print("Falsch!")
        points = points + 0
    

print("Sie haben " + str(points) + " von " + str(len(dict)) + " Vokabeln richtig beantwortet!")
                # !!!! Achtung: man nutzt fÃ¼r dictionaries len(dict), um elemente zu zÃ¤hlen.

    
