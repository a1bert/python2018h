## Aufgabeblatt Datatype

# e. / f.

def preisliste_eingaben ():
    preisliste = {}
    lebensmittel = str()
    preis = float()
        
    while lebensmittel != "x":
        lebensmittel = input("Lebensmittel eingeben: ")
        
        if lebensmittel == "x":
            break
        
        elif lebensmittel != "x":
            preis = input("Preis eingeben: ")
            preisliste[lebensmittel] = preis
            
    return preisliste

print("Dictionary:", preisliste_eingaben())
