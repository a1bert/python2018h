dict = {'Genf': 190000, 'Chur': 30000, 'ZuÌrich': 370000, 'Bern': 130000, 'Basel': 170000}

##Schreiben Sie ein Programm, dass eine Ausgabe
##nach folgendem Aufbau erzeugt:
##ZuÌrich hat 370000 Einwohner Genf hat 190000 Einwohner usw.

print("a.")

for stadt, einw in dict.items(): ## eine Klammer weil Befehl ruft items() fÃ¼r dict. stÃ¤dte
    print(stadt, "hat", einw, "Einwohner")
 
##AÌndern Sie das Programm so, dass die Liste sortiert nach keys ausgegeben wird.
##Heisser Tipp: list hat ja eine Methode sort()
    
print("b.")

key_list = list(dict.keys())
key_list.sort()

for stadt in key_list: ## hier gehÃ¶rt keine runde klammer, weil ist kein Befehl
    print(stadt, "hat", dict[stadt], "Einwohner")
    
# aufgabe b â Variante 2:
print ("b. Variante 2:")
stadt_liste = list(dict.items())
stadt_liste.sort()

for stadt, einw in stadt_liste:
    print(stadt, "hat", einw, "Einwohner")
    
# aufgabe b â Variante 3:
print ("b. Variante 3:")

for stadt, einw in sorted(dict.items()):
    ## dict.items() gibt uns eine liste: [[stadt, einw], ....]
    print(stadt, "hat", einw, "Einwohner")

##UÌberlegen Sie, wie man die Ausgabe sortiert nach Einwohnerzahlen programmieren koÌnnte.

print("c.")

einw_liste = [] ##zuert eine leere Liste erstellen
for stadt, einw in dict.items():
    einw_liste.append([einw, stadt])

einw_liste.sort()

# ausgabe: einwohner und stadt sind jetzt vertauscht!
for einw, stadt in einw_liste:
    print(stadt, "hat", einw, "Einwohner")

