## Autor: Almira Medaric
## Datum: 16.11.2018

## adhoc Übung 2.6
## b)

# Eingabe 
x = input("Bitte geben Sie eine Zahl ein")
x = int(x)

# Funktion für die Monate
def monat(y):
    while y < 1 or y > 12:
        y = int(input("Geben Sie bitte eine Zahl zwischen 1 und 12 ein"))
        
    if y == 1:
        monat = "Januar"
    
    elif y == 2:
        monat = "Februar"

    elif y == 3:
        monat = "März"
    
    elif y == 4:
        monat = "April"
        
    elif y == 5:
        monat = "Mai"
        
    elif y == 6:
        monat = "Juni"
        
    elif y == 7:
        monat = "July"
        
    elif y == 8:
        monat = "August"
        
    elif y == 9:
        monat = "September"
        
    elif y == 10:
        monat = "Oktober"
        
    elif y == 11:
        monat = "November"
        
    elif y == 12:
        monat = "Dezember"
    
    return monat
        
## Hauptprogramm:
## Achtung => Funktion muss immer mittels f() aufgeruden werden, sonst "<funtion .... >"
print(monat(x))

## Bedingung bis zur Ende
while x != 99:
    x = input("Bitte geben Sie eine weitere Zahl ein oder 99, um das Programmt zu beenden")
    x = int(x)
    
    if x == 99:
        print ("ENDE")
    
    else:
        print(monat(x))
    


