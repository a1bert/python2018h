# stundenwiederholung 30.11.18


# klasse machen mit 3 Klassenkamaraden
liste1 = ["alex", "gina", "remo"]
print(liste1)

# liste duplizieren
liste2 = liste1 * 2
#### oder:
liste3 = liste1 + liste1

# letzten beiden namen lÃ¶schen (mit eingabe)
liste1.remove("remo") ## mit remove kann man nur ein element eingeben !!!
liste1.remove("gina")

print(liste1)

# letzten beiden namen lÃ¶schen, ohne diese zu kennen
liste1 = ["alex", "gina", "remo"]
del liste1[1:]

liste4 = ["alex", "gina", "remo"]
del liste4[-2:] ## noch besser, weil funktionniert egal wie viele elemente man hat!

print(liste1)
print(liste4)

## ein namen durch neuen ersetzten
liste1 = ["alex", "gina", "remo"]
liste1.append("lisa")
liste1.remove("alex")

## ein namen durch neuen ersetzten
liste5 = ["alex", "gina", "remo"]
liste5[0]="lisa"

# geben sie alle elemente in liste aus (untereinander!!!!!!)
for ausgabe in liste1:
    print(ausgabe)
    #### so wird es in einer schleife ausgegeben, und somit immer in einer neue Zeile.

# Ã¤ndere das telefonnummer in einer liste
liste6 = [ ["Stephanie", 123], ["Remo", 456], ["Tamara", 789]]
liste6[1][1] = 899

# name mit telefonnummer ausgeben
liste6 = [ ["Stephanie", 123], ["Remo", 456], ["Tamara", 789]]
for name, tel in liste6:
    print(name, tel)

