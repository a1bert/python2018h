from turtle import *


anzahl_d = numinput("Anzahl Dreiecken", "Bitte geben Sie die Anzahl an Dreiecken Ihrer Wahl")
anzahl_r = numinput("Anzahl Reihen", "Bitte geben Sie die Anzahl an Reihen Ihrer Wahl")


def funktion_dreieck(size):
    for reihe in range (int(anzahl_r)):
        if reihe >= 1:
            penup()
            goto(0, (0 - (size * reihe)))
            pendown()
            
        for anzahl_dreiecken in range (int(anzahl_d)):
            pencolor("lightblue")
            fillcolor("lightblue")
            begin_fill()
            
            for jedes_dreieck in range (3):
                fd(size)
                lt(120)
            
            fd(size)
            penup()
            fd(size/3)
            pendown()
    
            end_fill()            

    reset()   
    
funktion_dreieck(40)
