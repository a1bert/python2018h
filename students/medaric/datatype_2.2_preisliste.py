## Aufgabeblatt Datatype

# 2.2
# a.
print("a.")

preisliste = {"Brot": 3.4, "Milch": 3.4, "Banane": 1, "Karotten": 1.8}
for lebensmittel, preis in preisliste.items():
     print(lebensmittel + " kostet " + str(preis) + " CHF.")

# b.
print("b.")
for lebensmittel, preis in sorted(preisliste.items()):
    print(lebensmittel + " kostet " + str(preis) + " CHF.")

# c.    
##print("c.")
##preise = []
##
##for lebensmittel, preis in preisliste.items():
##    preise.append([preis, lebensmittel])
##preise.sort()
##    
##for preis, lebensmittel in preise:
##    print(lebensmittel + " kostet " + str(preis) + " CHF.")
##
### d.
##print("d.")
##preise = []
##
##for lebensmittel, preis in preisliste.items():
##    preise.append([preis, lebensmittel])
##preise.sort(reverse = True)
##    
##for preis, lebensmittel in preise:
##    print(lebensmittel + " kostet " + str(preis) + " CHF.")
    
# e.
print("e.")
for lebensmittel, preis in preisliste.items():
    if preis < 2:
        print("Lebensmittel kostet weniger als 2 CHF:", lebensmittel)

# f.
print("f.")
for lebensmittel, preis in preisliste.items():
    if "en" in lebensmittel:
        print(lebensmittel)
        

