from math import *

def durschnitt_z(eingabe):
    maximum = max(eingabe)
    minimum = min(eingabe)
    durschnitt = sum(eingabe)/ len(eingabe)
    durschnitt = round(durschnitt, 2)
    return maximum, minimum, durschnitt

zahlenreihe = [1, 2, 3, 4, 5, 7]
print("Maximum:%s, Minimum: %s, und Durschnitt:%s" % durschnitt_z(zahlenreihe))
