def multi_reihe():
    
    for zahl1 in range (1, 11):
        print (str(zahl1)+"er Reihe:")
        
        for zahl2 in range (1,11):            
            total = zahl1 * zahl2
            print(str(zahl2) + "x" + str(zahl1) + "=" + str(total))
            
            zahl2 = zahl2 + 1
        
        zahl1 = zahl1 + 1
        print("........")
        
multi_reihe()