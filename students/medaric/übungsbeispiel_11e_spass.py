## 11. Spass mit Schleifn
## e)

def spass_e ():
    for zeichen in range (1):
        print("# " * 7)
    for zeichen in range (5):
        print("# " + "  " * 5 + "# ")
    for zeichen in range (1):
        print("# " * 7)

spass_e()

# Variante
print("=======")

def spass_evar ():
    for zeichen in range (7):
        
        if zeichen == 0 or zeichen == 6:
            print("# " * 7)          
        else:
            print("# " + "  " * 5 + "# ")
        

spass_evar()