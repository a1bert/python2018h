
def berechne_mahngebuehr(anzahl_ma, anzahl_me, post):
    if post == True:
        post = 2
        
        if anzahl_ma == 1:
            mahnung = (2 * anzahl_me) + post
        elif anzahl_ma == 2:
            mahnung = (4 * anzahl_me) + post
        elif anzahl_ma == 3:
            mahnung = (3 * anzahl_me) + post
        
    elif post == False:
        post = 0
        
        if anzahl_ma == 1:
            mahnung = (2 * anzahl_me)
        elif anzahl_ma == 2:
            mahnung = (4 * anzahl_me)
        elif anzahl_ma == 3:
            mahnung = (3 * anzahl_me)
    
    return mahnung


print(berechne_mahngebuehr(1, 5, False))
print(berechne_mahngebuehr(2, 5, True))