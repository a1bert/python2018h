## Leistungsaufgabe 5
## Gesamte Aufgabe für Buchstaben

from csv import writer
from urllib.request import urlopen

url = input("Bitte geben Sie eine URL ein: ")

if 'https://' in url:
    with urlopen(url) as f:
        content = f.read().decode('utf-8')
        content = content.lower()
        dict = {}
        for letter in content:
            if letter in dict:
                dict[letter] = dict[letter] + 1
            else:
                dict[letter] = 1
elif 'https://' not in url:
    with open(url) as f:
        content = f.read()
        content = content.lower()
        dict = {}
        for letter in content:
            if letter in dict:
                dict[letter] = dict[letter] + 1
            else:
                dict[letter] = 1


l = []

for letter, anzahl in dict.items():
    l.append([letter, anzahl])
    
l.sort()

with open("buchstabe.csv", 'w') as f:
    csv_writer = writer(f)
    for row in l:
       csv_writer.writerow(row)

## Gesamte Aufgabe für Wörter

from csv import writer

from urllib.request import urlopen
url = input("Bitte geben Sie eine URL ein: ")
if 'https://' in url:
    with urlopen(url) as f:
        content = f.read().decode('utf-8')
        content = content.lower().split()
        l = {}
        for wort in content:
            if wort in l:
                l[wort] = l[wort] + 1
            else:
                l[wort] = 1
if 'https://' not in url:
    with open(url) as f:
        file_content = f.read()
        l = {}
        for wort in content:
            if wort in l:
                l[wort] = l[wort] + 1
            else:
                l[wort] = 1
liste = []  
for wort, anzahl in l.items():
    liste.append([wort, anzahl])
liste.sort()
    

with open("wort.csv", 'w') as f:
    csv_writer = writer(f)
    for row in liste:
        csv_writer.writerow(row)