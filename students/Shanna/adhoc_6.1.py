## holt Dokument, welches man kopieren möchte. Es muss
## im selben Verzeichnis sein. R = read, es soll nur
## gelesen werden und wird in Variable 'a' gespeichert
## diese wird dan mit dem zweiten 'with' als neues .txt
## mit neuem Namen gespeichert.
## 1.
with open("adhoc_6.1.txt", 'r') as f:
    a = f.read() 
    
with open("adhoc_6.1_sicherung.txt", 'w') as f:
    f.write(a)
## 2.
i = 1
with open("adhoc_6.1.txt", 'r') as speichern:
    with open("adhoc_6.1.2.txt" , 'w') as sicherung:
        for zeile in speichern:
            zeile = str(i) + ": " + zeile
            sicherung.write(zeile)
            i = i + 1
## 3
try:
    with open("adhoc_6.1.txt", 'r') as speichern:
        with open("adhoc_6.1.3.txt" , 'w') as sicherung:
            for zeile in speichern:
                sicherung.write(zeile)
except FileNotFoundError:
    print("adhoc_6.2.txt file is missing")
except PermissionError:
    print("You ar not allowed to read adhoc_6.1.text")