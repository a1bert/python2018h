#ad hoc übung 6.1
try: 
    with open("adressbuch.py", "r") as source:  #öffnet dok, das gespeichert werden soll ("r" = default)
        with open("adressbuch_sicherung.py", "w") as save: # erstellt und öffnet kopie (immer "w" benutzen!)
            i = 0
            for zeile in source: #erweiterung der kopie durch zeilennr + ":" + zeile
                zeilennr = str(i) 
                save.write(zeilennr + ": " + zeile) #schreibt die jeweilige zeile plus zeilennr
                i = i + 1 #addiert +1 zu zeilennr
                
except FileNotFoundError: #falls zieldokument nicht gefunden wird
    print("Das gewünschte Dokument konnte nicht gefunden werden.")

except PermissionError: #falls zieldokument nicht gelesen werden kann
    print("Das gewünschte Dokument konnte nicht geöffnet werden.")

