#leistungsaufgabe 3b
dict = {"Zahnarzt":"dentist", "Haus":"house", "Schere":"scissors", "Pflanze":"plant", "Fenster":"window", "Tür":"door", "Stuhl":"chair", "Tisch":"table", "Kissen":"pillow", "Uhr":"clock"}
eingabe = ""
richtig = [] #neue liste für abfragestatistik

for deutsch, englisch in dict.items():
    print("Was heisst", deutsch +"?")
    eingabe = input()
    if eingabe == englisch:
        richtig.append(eingabe)
        print("RICHTIG!")
        
    
    elif eingabe != englisch:
        print("FALSCH!")
    
print("Sie haben", len(richtig), "von 10 Vokabeln richtig beantwortet!")
