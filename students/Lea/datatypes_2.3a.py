#datatypes 2.3a

#liste aus 2.1
preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
preisliste["Milch"] = 2.05
preisliste["Tee"] = 4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1

#a: liste filtern, alle lebensmittel aus dem dict entfernen, die weniger als 2.5 kosten
preisliste_neu = {} #neue Liste erstellen
for name, preis in preisliste.items():
    if preis >= 2.5: 
        preisliste_neu[name] = preis

for name, preis in preisliste_neu.items():
    print(name, "kostet", preis, "CHF.")

