#leistungsaufgabe 5
#teil 1: ressource öffnen
from urllib.request import urlopen #benötigt, um urls zu öffnen
from csv import writer #benötigt, um csv zu schreiben
   
eingabe = input("Bitte geben Sie eine URL oder Datei als Ressource an: ") #zu lesende ressource entgegennehmen

#teil 1a: webressource öffnen
if eingabe[0:8] == "https://": #falls eingabe eine webressource ist
    with urlopen(eingabe) as source:
        content = source.read().decode('utf-8')
    content = content.lower() #alles klein schreiben
    content = content.replace(".", "").replace(",", "").replace(";", "").replace(":", "").replace("?", "").replace("!", "").replace("(", "").replace(")", "").replace("[", "").replace("]", "").replace("-", "").replace("<", "").replace(">", "").replace("=", "").replace("'", "").replace("\n", "").replace("#", "").replace("/", "").replace("&", "").replace("+", "").replace("|", "").replace("@", "").replace("0", "").replace("1", "").replace("2", "").replace("3", "").replace("4", "").replace("5", "").replace("6", "").replace("7", "").replace("8", "").replace("9", "").replace("«", "").replace("»", "").replace("{", "").replace("}", "").replace("*", "").replace('"', '').replace("_", "").replace("%", "").replace("½", "") #sonderzeichen und zahlen entfernen, aber nicht vollständig geklappt
    #gibt es eine elegantere lösung als mit replace???
    wortliste = content.split() #macht eine liste aus allen wörtern der webressource

#teil 1b: datei öffnen
else: #falls eingabe eine datei ist, analoger vorgang wie bei webressource
    with open(eingabe, "r") as file:
        content = file.read() #liest die Datei
        content = content.lower() #alles kleingeschrieben
        content = content.replace(".", "").replace(",", "").replace(";", "").replace(":", "").replace("?", "").replace("!", "").replace("(", "").replace(")", "").replace("[", "").replace("]", "").replace("-", "").replace("<", "").replace(">", "").replace("=", "").replace("'", "").replace("\n", "").replace("#", "").replace("/", "").replace("&", "").replace("+", "").replace("|", "").replace("@", "").replace("0", "").replace("1", "").replace("2", "").replace("3", "").replace("4", "").replace("5", "").replace("6", "").replace("7", "").replace("8", "").replace("9", "").replace("«", "").replace("»", "").replace("{", "").replace("}", "").replace("*", "").replace('"', '').replace("_", "").replace("%", "").replace("½", "") #sonderzeichen und zahlen entfernen, aber nicht vollständig geklappt
        wortliste = content.split() #liste aus allen wörtern


#teil 2a: wortstatistik erstellen (vgl. hausübungen auf den 7.12.)
wortstatistik = {} #leeres dict, in dem die wörter gezählt werden (wort = key, vorkommen = value)
    
for wort in wortliste: #für alle wörter in der wortliste
    if wort in wortstatistik: #falls das wort in der liste vorkommt:
        wortstatistik[wort] = wortstatistik[wort] + 1 #wenn wort wiederholt vorkommt -> statistik zählt wort = wort +1
    else:
        wortstatistik[wort] = 1 #wenn wort das erste mal vorkommt -> statistik zählt wort=1
            
#teil 2b: buchstabenstatistik erstellen
buchstabenliste = list(content) #splittet text in einzelne buchstaben auf
buch_statistik = {} #leeres dict für die buchstabenstatistik

for buchstabe in buchstabenliste: #für alle buchstaben in der buchstabenliste: analog zu wortstatistik
    if buchstabe in buchstabenliste:
        buch_statistik[buchstabe] = buchstabenliste.count(buchstabe) #zählt vorkommen der buchstaben in buchstabenliste, wird in dict gespeichert
        #im resultat wird es aber immer noch sonderzeichen dabei haben, die ich oben nicht herausgefiltert habe!

#teil 3a: wortstatistik in csv datei ausgeben
w_values = [] #leere liste für wörter der statistik
for key, value in wortstatistik.items(): #schreibt wörter + ihr vorkommen in die liste ein
    w_values.append([key, value])

with open('url_statistik_wort.csv', 'w') as f: #erstellt eine csv-datei mit der wortstatistik
    csv_writer = writer(f)
    for row in w_values:
        csv_writer.writerow(row)
        
#teil 3b: buchstabenstatistik in csv datei ausgeben
b_values = []
for key, value in buch_statistik.items():
    b_values.append([key, value])
    
with open('url_statistik_buchstabe.csv', 'w') as f: #erstellt eine csv-datei mit der buchstabenstatistik
    csv_writer = writer(f)
    for row in b_values:
        csv_writer.writerow(row)
