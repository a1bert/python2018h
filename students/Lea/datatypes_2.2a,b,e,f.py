#datatypes 2.2
#liste aus 2.1
preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
preisliste["Milch"] = 2.05
preisliste["Tee"] = 4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1

#ausgabe a: normal
print("a")
for name, preis in preisliste.items():
    print(name, "kostet", preis, "CHF.")

#ausgabe b: alphabetisch sortiert
print("b")
for name, preis in sorted(preisliste.items()):
    print(name, "kostet", preis, "CHF.")

#ausgabe e: nur lebensmittel ausgeben, deren preis kleiner als 2 CHF ist
print("e")
for name, preis in preisliste.items():
    if preis < 2.0:   #gibt nichts aus, da kein preis < 2 CHF         
        print(name, "kostet", preis, "CHF.")
    if preis < 2.5:   #alternative, klappt        
        print(name, "kostet", preis, "CHF.")

#ausgabe f: nur lebensmittel mit Zeichenkette "en" drin
print("f")
for name, preis in preisliste.items():
    if "en" in name:
        print(name, "kostet", preis, "CHF.")        

