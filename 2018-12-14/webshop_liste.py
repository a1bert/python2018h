from csv import writer

artikel_liste = [{"id":"A0381",
                         "bez": "Abenteuer von Tim",
                         "typ":"Spiel",
                         "preis": 20,
                         "hersteller": "Ubisoft"},
                 {"id":"BC001",
                         "bez": "Ein Panda ist kein Känguru",
                         "typ":"Audio",
                         "preis": 10,
                         "hersteller": "Tanja"}]

def dictionary_zu_list(dictionary):
    liste = []
    for key in "id", "bez", "preis":
        liste.append(dictionary[key])
        
    return liste


# variante 2
# article_list = [{...}, {....}]

suchterm = input("Suche nach? ")
suchterm = suchterm.lower()
# variante 2
for artikel in artikel_liste:
    for artikel_beschreibung in artikel.values():
        # print(artikel_beschreibung)
        artikel_beschreibung = str(artikel_beschreibung)
        if suchterm in artikel_beschreibung.lower():
            # wandle den artikel (dictionary) in die entsprechende ergebnis liste um
            ergebnis_liste = dictionary_zu_list(artikel)
            print(ergebnis_liste)
            break

# speichere liste in CSV file
with open("artikel-liste.csv", "w") as f:
    csv_writer = writer(f)
    for artikel in artikel_liste:
        # wichtig: ich kann kein dictionary sondern nur listen speichern
        artikel_eintrag = dictionary_zu_list(artikel)
        csv_writer.writerow(artikel_eintrag)
        
