# variante a
with open("text.txt", "r") as f:
    for line in f:
        print(line)
        
# variante b
with open("text.txt", "r") as f:
    inhalt = f.read()
    # print(f)
    print(inhalt)
    zeilen = inhalt.split("\n")
    print(zeilen)
##    
##    for line in zeilen:
##        print(line)

# https://www.gutenberg.org/cache/epub/3296/pg3296.txt
from urllib.request import urlopen
with urlopen("https://www.gutenberg.org/cache/epub/3296/pg3296.txt") as source:
    with open("beispiel.html", "w") as destination:
        web_content = source.read().decode("utf8")
        
        # alles kleingeschrieben, damit wir grossgeschriebene instanzen
        # nicht "übersehen"
        web_content = web_content.lower()
        
        # cleanup operations
        web_content = web_content.replace(",", " ").replace(".", " ").replace("!", " ").replace("?", " ")
        word_list = web_content.split()
        print("faith" ,word_list.count("faith")) 
        print("alle wörter",len(word_list))
        
        destination.write(web_content)
