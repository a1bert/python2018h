l = [] # Einkaufsliste, noch ist sie leer

def items_eingeben():
    while True:
        abort = input("Was soll noch auf die Liste?  \n Mit x zurück \n")
        if abort == "x":
            return
        l.append(abort)

def items_ausgeben():
    l.sort()
    print("Einkaufsliste :D \n")
    i = 1
    for item in l:
        print(i, item)
        i = i + 1
    print(" ")
        

def items_korrigieren():  # User kontrolliert und korrigiert
    items_ausgeben()
    itemnr = int(input("Welches Item soll geändert werden? ")) - 1  # minus 1 weil der pc mit 0 anfängt zu zählen
    new_item = input("Was soll es neu sein? x für nix. \n")
    del l[itemnr]
    if new_item != "x":
        l.insert(itemnr, new_item)
        
# Einkaufsliste wird gespeichert
def jesus(liste):
    with open("einkaufsliste.txt", "w") as save:
        for obj in liste:
            save.write(obj)   # zeile = obj + "\n"  save.write(zeile)
            save.write("\n")  # neue Zeile, damit nicht alle Items aneinander gehängt werden a la BierButterKäse

# Einkaufsliste wird eingelsesen
def reload():
    try:
        with open("einkaufsliste.txt") as old:
            for line in old:
                line = line.strip() # strip for me ;)
                l.append(line)
    except FileNotFoundError:
            print("Keine Einkaufsliste gefunden. Neue wird angelegt. \n \n")


# menu 1 Eingeben, 2 Prüfen (korrigieren), 3 ausgeben, 0 exit
def menu():
    while True:
        choice = int(input("Options \n 1 - Eingabe \n 2 - Korrigieren \n 3 - Ausgabe \n 0 - Speichern und Exit \n"))
        if choice == 0:
            jesus(l)  # Jesus saves ;)
            break
        elif choice == 1:
            items_eingeben()
        elif choice == 2:
            items_korrigieren()
        elif choice == 3:
            items_ausgeben()

# hier beginnt der PC etwas zu tun
reload() # lädt die gespeicherte Liste auf l
menu()