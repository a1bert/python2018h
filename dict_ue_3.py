# dict_uebung 3a
'''
Die \International Civil Aviation Organization (ICAO) Alphabet weist
Buchstaben Wortern zu1, damit diese fehlerfrei buchstabiert werden
konnen. Schreiben Sie ein Programm, das ein beliebiges Wort buchstabiert.
Welches Wort soll ich buchstabieren: Synthese
Ausgabe: Sierra-Yankee-November-Tango-Hotel-Echo-Sierra-Echo
'''
def alpha_icao(): #vorbereitung dictionary
    icao = {}
    prep= ['A: Alfa', 'B: Bravo', 'C: Charlie', 'D: Delta', 'E: Echo', 'F: Foxtrot', 'G: Golf', 'H: Hotel', 'I: India',
           'J: Juliett', 'K: Kilo', 'L: Lima', 'M: Mike', 'N: November', 'O: Oscar', 'P: Papa', 'Q: Quebec', 'R: Romeo',
           'S: Sierra', 'T: Tango', 'U: Uniform', 'V: Victor', 'W: Whiskey', 'X: X-ray', 'Y: Yankee', 'Z: Zulu'] #liste von website, mit .split(" -") vorbereitet
    for i in prep:
        key = i[0].lower()
        icao[key] = i[3:]
    print(icao)
    return icao

def spelling_bee(alpha):
    eingabe = input("Welches Wort soll ich buchstabieren? ")
    spell = eingabe.lower()
    ausgabe = ""
    for letter in spell:
        for index, word in alpha.items():
            if letter == index:
                ausgabe = ausgabe+word+"-"
                continue
        if letter not in alpha.keys():
            print("Kann %s nicht buchstabieren, da %s nicht definiert wurde." % (eingabe, letter))
            return       
                
    #[:-1] entfernt den letzten strich von der schleife
    print(ausgabe[:-1])
    return ausgabe
        
            
        
    
    return

spelling_bee(alpha_icao())

