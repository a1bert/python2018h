# A hello world program
print("Hello World!")

# Beispiel a)
print("D'Amico, Giulia, Spitalstrasse, 32, 4056, Basel")

vorname = "Max"
nachname = "Mustermann"
strasse = "Straussengasse 5"
plz = 7000
ort = "Chur"

print (vorname, nachname, strasse, plz, ort)

# Beispiel b)
print(1+2+3+4+5)

print(sum([1, 2, 3, 4, 5]))


# Beispiel c)
FLIEGEN = "fliegen "
print("wenn", FLIEGEN+ "hinter", FLIEGEN *5+ "hinterher")

# Beispiel d)
wort="Hello"
print(wort[4]+wort[3]+wort[2]+wort[1]+wort[0])
print(wort[::-1])

# Funktionsweise:
#   variable[startindex:endindex:schrittweite]
#   wenn startindex nicht angegeben wird, ist dieser 0
#   wenn endindex nicht angegeben wird, ist dieser der letzte index des strings
#   wenn schrittweite nicht angegeben wird, ist diese 1
print(wort[0:5:-1])
print(wort[::])
print(wort[::2])