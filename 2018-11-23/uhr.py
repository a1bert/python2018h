for tag in "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag":
    for stunde in range(24):
        for minute in range(60):
            # variante 1
            #print(tag, str(stunde) + ":" + str(minute))
            
            # variante 2
            print("Tag: %s, Zeit: %d:%.2d" % (tag, stunde, minute))