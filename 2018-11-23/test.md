Fehler
======

* Case 1

  ```python
  def summe(a, b, c):
    a= input("1. ")  # Strings statt floats
    b= input("2. ")
    c= input("3. ")
    return a+b+c

  # a, b, c sind an diesem Punkt nicht definiert worden.
  print("Summe :", summe(a, b, c)) 
  ```

* "legale" Varianten
  ```python
  x = float(input("1. "))
  y = float(input("2. "))
  z = float(input("3. "))

  def summe(a, b, c):
      return a+b+c

  # a, b, c sind an diesem Punkt nicht definiert worden.
  print("Summe :", summe(x, y, z)) 
  ```

* Case 2

  ```python
  def haus(groesse):
     ...

  Haus(200)

  ```
* ```python
  print("Volumen: "+ str(round(volumen), 3) + "m3")
  print("Volumen:" + berechne_volumen(2,1)
  ```

Schönheitsprobleme ohne Punkteabzug
===================================
* Case 1
  ```python
  def summe():
    a= float(input("1. "))
    b= float(input("2. "))
    c= float(input("3. "))
    return a+b+c

  print("Summe:", summe())
  ```

* Case 2
  ```python
  print(haus_mit_rahmen())
  ```
        
