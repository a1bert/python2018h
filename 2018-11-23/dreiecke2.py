from turtle import *


def dreieck(s):
    for seite in range(3):
        forward(s)
        left(120)
        
def neue_zeile(s, delta, count_in_row):
    """
    wir gehen in die neue Zeile und danach in die Pause...
    """
    penup()
    right(90)
    forward(100)
    right(90)
    forward((delta + s) * count_in_row)
    right(180)
    pendown()
        
def multi_dreieck(count, s, delta, count_in_row):
    """
    count ... anzahl an dreiecken
    s ... seitenlänge
    delta ... abstand zwischen den dreiecken
    count_in_row ... anzahl von dreiecken in einer zeile
    """
      
    for dreieck_nr in range(count):
        dreieck(s)
        penup()
        forward(delta + s)
        pendown()
        
        if dreieck_nr !=0 and (dreieck_nr + 1) % count_in_row == 0:
            neue_zeile(s, delta, count_in_row)
            
    
        
seitenlänge = 100
anzahl = 7
anzahl_in_zeile = 3
multi_dreieck(anzahl, seitenlänge, 10, anzahl_in_zeile)

