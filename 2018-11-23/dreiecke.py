from turtle import *


def dreieck(s):
    for seite in range(3):
        forward(s)
        left(120)
        
def multi_dreieck(count, s, delta, count_in_row):
    """
    count ... anzahl an dreiecken
    s ... seitenlänge
    delta ... abstand zwischen den dreiecken
    count_in_row ... anzahl von dreiecken in einer zeile
    """
    
    painted_triangle = 0
    
    for dreieck_nr in range(count):
        for i in range(count_in_row):
            dreieck(s)
            penup()
            forward(delta + s)
            pendown()
            painted_triangle = painted_triangle + 1
            
            if painted_triangle == count:
                return 
            
        penup()
        right(90)
        forward(100)
        right(90)
        forward((delta + s) * count_in_row)
        right(180)
        pendown()
    
        
seitenlänge = 100
anzahl = 7
anzahl_in_zeile = 3
multi_dreieck(anzahl, seitenlänge, 10, anzahl_in_zeile)
