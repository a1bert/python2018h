#leistungsaufgabe 3b
dict = {"Zahnarzt":"dentist", "Haus":"house", "Schere":"scissors", "Pflanze":"plant", "Fenster":"window", "Tür":"door", "Stuhl":"chair", "Tisch":"table", "Kissen":"pillow", "Uhr":"clock"}
eingabe = ""
richtig = [] #neue liste für abfragestatistik

count = 0 # alternative möglichkeit die richtigen antworten zu zählen
for deutsch, englisch in dict.items():
    eingabe = input("Was heisst " + deutsch +"? ")
    if eingabe == englisch:
        richtig.append(eingabe)
        count = count + 1
        print("RICHTIG!")
        
    
    elif eingabe != englisch:
        print("FALSCH!")
    
print("Sie haben", len(richtig), "von 10 Vokabeln richtig beantwortet!")
# ausgabe der richtigen antworten über die alternative möglichkeit
print("Sie haben", count, "von 10 Vokabeln richtig beantwortet!")
