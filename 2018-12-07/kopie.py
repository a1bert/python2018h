
with open("adressbuch.py", "r") as source:  #öffnet das ZU SPEICHERNDE Dokument
    with open("adressbuch_sicherung.py", "w") as save: # kreiert und öffnet
                                                       # das Zieldokument
        for zeile in source:  # iteriert über alle Zeile des adressbuches
            save.write(zeile)  # speichert die jeweilige Zeile



# variante 2
with open("adressbuch.py") as source:
    data = source.read() # lese den kompletten inhalt von "adressbuch.py" in die variable data ein
    
with open("adressbuch_sicherungs2.py", "w") as dest:
    print("Hallo") 
    dest.write(data)
    
